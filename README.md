# Archival PDF files for the _World Music Textbook_ project

This repository includes the files, templates, and scripts that we use to create the archival PDF versions of _World Music Textbook_ chapters. These require `pandoc` and possibly some other installed plugins, read any error messages or contact the editors with any questions.

For more on the _World Music Textbook_ project, [see the homepage](https://worldmusictextbook.org/) or the [GitLab repository](https://gitlab.com/world-music-textbook/wmt).

_Note: this was edited after our move to the website's second version in November 2022. Any chapters from before that may not work out of the box because of the changed directory structures. Copying those files—like assets—to the proper places and checking the markdown files for any similar adjustments should fix any issues in creating new PDFs for these older chapters._

## Initial setup

Install LaTeX (do a search for the best way to go about this depending on your system), then add the required packaages for this template. On a Mac, using `tlmgr`, this command is:

```
tlmgr install hanging xurl collection-fontsrecommended palatino fpl
```

\*Note: I did this through troubleshooting, so adding one of these may limit the need to do all of them. `fpl` and `palatino` might be in the `collection-` package.

Copy any image files into the `assets` folder. Get tex conversion from the markdown file:

```
pandoc ./chapters/nielsen-2020/index.md -o ./chapters/nielsen-2020/nielsen-body.tex --resource-path=./assets/
```

Remove any unnecessary items like buttons or trailing headings (`## Notes`). Copy the `tex` headers from the template file, then update the metadata:

- Title, author, affiliation
- Date, DOI, any changes to copyright
- Abstract, keywords

### Online version URL callout

Create the URL information using the following:

```
\providecommand{\wmturl}{\href{https://worldmusictextbook.org/alexander-2024b}{https://worldmusictextbook.org/alexan der-2024b}}
\providecommand{\wmturltext}{
  \noindent\emph{The online version of this chapter is available at \wmturl.}
}
```

The text of the URL may need a space to allow for a break since it uses the `href` and not the `url` command (which automatically breaks). This keeps it italicized and looking nicer, but does not format it properly.

## General edits to the tex file

1. Remove `tightlist`
2. Remove `sub` from each `section` to level it up one and add `*` to remove section numbering (use find/replace to change `subsection` to `section*`)

## Figures

Change figures to follow this format (including adding `[width=\textwidth]`) and remove any "Figure 1" from the captions (they get added automatically):

```tex
\begin{figure}
  \includegraphics[width=\textwidth]{nielsen-fig1.jpg}
  \caption{Mexica New Year Ceremony in San Jose, California in March 2016.}
\end{figure}
```

### Videos

These use a placeholder image and make use of a link to the site's version of the chapter. Include the following after the `wmturl` command definition in the header:

```tex
\providecommand{\wmturlcaption}{
  Visit \href{https://worldmusictextbook.org/sur-2023}{the website} to view video examples.
}
```

Change videos to follow this format (note `caption*` and the `\wmturlcaption` command):

```tex
\begin{figure}
  \includegraphics[width=\textwidth]{../../assets/play-video.png}
  \caption*{Video Example 1. Example of a dance from the 2016 Mexica New Year
  Ceremony in San Jose, California hosted by Calpulli Tonalehqueh. \wmturlcaption}
\end{figure}
```

If we use QR codes instead:

- Create redirect page (fix YouTube link from `embed` to `watch=`)
- Use this page to create the QR codes: [QR Code generator library](https://www.nayuki.io/page/qr-code-generator-library)
- Set to create vector image with a border of 1, then get a screen shot

```tex
\begin{figure}
  \centering
  \includegraphics[height=4cm]{qr-code.png}
  \caption*{Video Example 1. Example of a dance from the 2016 Mexica New Year
  Ceremony in San Jose, California hosted by Calpulli Tonalehqueh. \wmturlcaption}
\end{figure}
```

## Bibliography

1. Replace `\{:.hang\}` with ``
2. Check for invisible starting characters before `------` author names
3. Enclose bibliography in `\begin{hangparas}{15pt}{1}`
4. Use `\hangpara{15pt}{1}` with other hanging needs so that the spacing before other sections does not get mixed up
5. Use `\vspace{16pt} %5mm vertical space` to add space before any following section headers

## Hyphenation and layout errors

Fix `hbox` errors by defining hyphens between syllables using `\-`. Where this does not work, use `raggedright` or, if necessary, enclose the paragraph using `sloppy`:

```
\begin{sloppypar}
  TEXT
\end{sloppypar}
```

For `vbox` errors, try `raggedbottom`, which will hopefully avoid having to use more extensive formatting options.

## Build the file

Use `xelatex` to build:

```
xelatex nielsen-2020.tex
```

Note: using `pdflatex` to build the file instead may solve some missing character errors.

Remove any unnecessary files and send the PDF along for proofing.
