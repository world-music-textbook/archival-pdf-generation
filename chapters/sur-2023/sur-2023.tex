\documentclass[twoside]{article}
\usepackage{mathpazo}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{xurl}
\usepackage[colorlinks]{hyperref}
\hypersetup{
  colorlinks,
  urlcolor=blue,
  linkcolor=black
}
\usepackage{caption} % unnumbered video examples
% \usepackage{authblk} % author affiliations
\usepackage{hanging} % hanging indents

% headers
\usepackage{fancyhdr}
\renewcommand{\headrulewidth}{0pt}

% first page footer
\fancypagestyle{infofooter}{%
  \fancyhf{}
  \renewcommand\headrulewidth{0pt}
  \fancyfoot[L]{\sffamily\small 
    WORLD MUSIC TEXTBOOK, ISSN: 2767-4215; \copyright~2023, CC-BY-NC-ND\\
    https://doi.org/10.25035/wmt.2023.002}
}
\thispagestyle{infofooter} % remove header from first page
\pagestyle{fancy}     % add headers in other pages

% normal headers
\fancyhead[LO]{\sffamily\small \textbf{\thepage} \quad World Music Textbook}
\fancyhead[RE]{\sffamily\small Sur et al.: Oh Ol Woman Blong Wota \quad \textbf{\thepage}}
\fancyfoot{}

% flush left title
\makeatletter
\renewcommand{\maketitle}{\bgroup\setlength{\parindent}{0pt}
\begin{flushleft}
  \vspace*{3\baselineskip}
  \huge{\textbf{\@title}}

  \medskip
  
  \large{\@author}
\end{flushleft}\egroup
}
\makeatother

% for keywords and abstract
\providecommand{\abstracttext}[1]
{
  \noindent
  \textbf{Abstract:} #1
}

\providecommand{\keywords}[1]
{
  \newline
  \textbf{Keywords:} #1
}

% for link
\providecommand{\wmturl}{\href{https://worldmusictextbook.org/sur-2023}{https://worldmusictextbook.org/sur-2023}}
\providecommand{\wmturltext}{
  \noindent\emph{The online version of this chapter includes all embedded content and is available at \wmturl.}
}
\providecommand{\wmturlcaption}{
  Visit \href{https://worldmusictextbook.org/sur-2023}{the website} to view video examples.
}

% metadata
\title{Oh Ol Woman Blong Wota (The Women of the Water)}
\author{Sandy SUR | Leweton Cultural Village\\Ashley Burgess | Griffith University\\Maeve McKenna | Independent filmmaker\\Catherine Grant | Griffith University}
% \affil{}

\date{}

% document
\begin{document}
\suppressfloats % prevent float above title
\maketitle

\abstracttext{The women of Leweton have been performing Water Music for international audiences since the founding of the Leweton Cultural Village in 2008, and have been practising this tradition for as long as they remember.}
\keywords{activism, heritage, sustainability, Pacific}

\smallskip

\wmturltext

\medskip

\noindent\hfil\rule{0.5\textwidth}{0.4pt}\hfil

\bigskip

\hypertarget{etuxebtung-vanuatu-womens-water-music}{%
\section*{Etëtung: Vanuatu Women's Water
Music}\label{etuxebtung-vanuatu-womens-water-music}}

In the village of Leweton on the tropical island of Espiritu Santo in
Vanuatu, a community is rallying together to revitalise and share its
cultural and environmental knowledge across generations. Its people come
from the smaller, more remote northern islands of Merelava and Gaua, two
of the 83 islands in the archipelago that comprises the Pacific nation
of Vanuatu. Over time, these people relocated to Leweton, partly to
access education, employment, healthcare and other services available on
Espiritu Santo, but also as a result of the difficulties the rapidly
changing climate presented to lives and livelihoods (Hayward 2014, Grant
2019).

Throughout Vanuatu, the climate crisis is wreaking environmental havoc.
Cyclones in 2015 and 2020 were the two strongest ever recorded in the
nation, each leaving a good proportion of the population in need of
humanitarian assistance (United Nations Children's Fund 2016, World
Vision 2020). Natural disasters like these are increasing in frequency
and severity due to the climate crisis (Intergovernmental Panel on
Climate Change 2021). They threaten traditional ways of life in Vanuatu
and disrupt time-honoured cultural practices.

\begin{figure}
  \includegraphics[width=\textwidth]{sur-2023-fig-1.jpg}
  \caption{Destruction caused by Cyclone Harold to Leweton
  village near Luganville on the island of Espiritu Santo, Vanuatu. Photo:
  Sandy Sur, 9 April 2020. Used with permission.}
\end{figure}

The Leweton community recognises, however, that the treasured cultural
practices of their home islands are a way of both understanding and
responding to changes in the climate. One such practice is
\emph{Etëtung}, known in English as the Vanuatu Women's Water Music.
\emph{Etëtung} is a sonically and visually striking performance
tradition with deep interconnections to the natural world. Standing in a
circle or semi-circle in waist-deep water, the women rhythmically slap
the surface of the water using a variety of hand techniques that create
a surprising diversity of sounds. These sounds reflect those in the
natural local environment---the call of a particular species of bird; an
oncoming cyclone; or waves over the reef at low tide (Dick 2014).

Contemporary \emph{Etëtung} performances often comprise a handful of
``pieces'' each lasting a minute or two, resulting in a diverse set of
around five or ten minutes (though this depends on performance context
and function). Typically, each piece features a distinct rhythm (or set
of rhythms) accompanied by a simple chanted or intoned melodic phrase.
Video 1 shows the women and girls of Leweton performing one such piece
in the coastal shallows near their village; with their words, they call
for rain, and (in the words) the rain comes.

\begin{figure}
  \includegraphics[width=\textwidth]{../../assets/play-video.png}
  \caption*{Video Example 1. Women and girls of Leweton village perform Etëtung in
  the coastal shallows near Leweton, Espiritu Santo, Vanuatu, 17 November
  2017. Video: Ashley Burgess. \wmturlcaption}
\end{figure}

In performance, \emph{Etëtung} becomes a message passing through the air
and water, simultaneously reflecting and shaping the space around it.
For the Leweton community, \emph{Etëtung} is an `acoustemology:' a sonic
way of knowing and interacting with the natural world, and a means to
strengthen and celebrate the deep relationship between nature and
culture (Feld 2012). In the words of cultural leader Sandy Sur:

\begin{quote}
\sloppy The sound weaves through the ocean and the land, weaving our stories as
it goes. Our stories are told through the water like a book {[}\ldots{]}
When the women are performing the Water Music, they're telling us our
story through nature. (Interview with Catherine Grant, November 2017)
\end{quote}

An act of deep resilience, \emph{Etëtung} is a medium that allows the
people of Leweton to collaboratively acknowledge, reflect on, and
respond to the environmental and climate changes they are experiencing.
Performer Cecelia Lolonun says,

\begin{quote}
The sounds of Water Music are still the same, but the meaning of Water
Music is different now, because of climate change. Water gives us the
sounds of things that are happening now {[}\ldots{]} Climate change
changes everything about nature and us. We have to keep on teaching
traditional ways of looking after things. If we keep these ways strong,
we can keep our nature alive. (Interview with Catherine Grant, November
2017)
\end{quote}

\emph{Etëtung} is also a way for Leweton villagers to share their
cultural and environmental knowledge with tourists, and to educate them
about the local impacts of environmental and climate change. From nearby
Luganville port, a popular stop for cruise ships, tourists make the
short trip to Leweton to participate in the activities of Leweton
Cultural Village, an initiative Sandy Sur founded in the mid-2000s to
support local livelihoods and ``kastom'' (traditional or heritage)
cultural practices. \emph{Etëtung} is the signature cultural practice
that the villagers showcase for tourists.

In recent years, as the international profile of \emph{Etëtung} has
grown, the women of Leweton have been invited to perform around the
world (Dick 2014; Hayward 2014). In this way, \emph{Etëtung} has enabled
their participation in global climate discussions. The women proudly
showcase their unique cultural practice as a way to generate wider
awareness about the ramifications of climate change for Indigenous and
island communities around the world (Grant 2019). In an era where calls
for urgent climate action are often accused of being hyperbolic,
\emph{Etëtung} is a strikingly unique and powerful way to educate and
rally for climate action.

\hypertarget{the-film}{%
\section*{The Film}\label{the-film}}

The following film (Video 2) is an outcome of long-term and ongoing
partnerships between three Brisbane-based Australian creative arts
researchers---documentary filmmaker Ashley Burgess, director Maeve
McKenna, and music researcher Catherine Grant -- and the members of the
Leweton community, including our primary contact and co-author Leweton
cultural leader Sandy Sur. Catherine Grant had previously collaborated
with Sandy and the women of Leweton on a project that explored the
relationship of \emph{Etëtung} to issues of climate justice (2017-2018,
reported in Grant 2019).

The film was recorded in 2019 during Ashley's and Maeve's trip to
Leweton, during which they were leading an international study abroad
tour for students at their university. Ashley and Maeve consulted with
community members in advance about the topic and content of the film.
Most of those they consulted considered \emph{Etëtung} to be a defining
part of their village identity and an important part of their culture,
and wanted to celebrate and share its deeper meaning.

All three of us Australian researchers (Ashley, Maeve, and Catherine)
continue our collaboration with Sandy Sur and the people of Leweton at
the time of writing (late 2022). According to Sandy, research, creative,
and educational collaborations like these leverage the efforts of the
Leweton community. They help the story of Leweton's cultural and natural
richness reach a wide international audience. They help people learn
about how meaningful and important these interconnections are for the
people of Leweton. They also help other people understand how deeply
water, nature, and etuxebtung-vanuatu-womens-water-music are interconnected, not only for Indigenous
peoples in Vanuatu, but for everyone around the world.

\begin{figure}
  \includegraphics[width=\textwidth]{../../assets/play-video.png}
  \caption*{Video Example 2. Ol Woman Blong Wota (The Women of the Water) {[}Short
  film{]}. Producer: Ashley Burgess. Director: Maeve McKenna. Filmed at
  Leweton, Espiritu Santo, Vanuatu, January 2019. \wmturlcaption}
\end{figure}

\hypertarget{additional-materials}{%
\section*{Additional Materials}\label{additional-materials}}

\hypertarget{acknowledgements}{%
\subsection*{Acknowledgements}\label{acknowledgements}}

Ashley Burgess and Maeve McKenna's 2019 field trip to Leweton on which
this film was made was funded by a grant of the Australian Government's
New Colombo Plan. Catherine Grant's field trip in 2017 was funded by a
research grant from Griffith University. The authors wish to thank all
members of the Leweton community for sharing their unique cultural
practices and knowledge with the wider world. The women performers who feature in this film are Denilla Frazer, Melinda Frazer, Jerolyn Frazer, Beverley Frazer, Cecilia Tingris, Cicilia Wari, Marie Sur, Sonrin Sur, Trisha Sur, and Margaret Tingris.

\hypertarget{discussion-questions}{%
\subsection*{Discussion Questions}\label{discussion-questions}}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\item
  \sloppy Apart from cyclones, what other challenges might the climate crisis be
  generating for the people of small-island communities like Leweton?
  What impacts might those challenges be having on water, reefs, land,
  food, tourism, health, housing, employment, cultural practices, and
  other aspects of life?
\item
  The video uses the phrase ``Water is life.'' What could this mean for
  the people of Leweton? What social and cultural functions of water
  does this idea suggest for the Leweton community?
\item
  \emph{Etëtung} is a gendered practice. From the video, what roles
  might \emph{Etëtung} play in the lives of women and girls of Leweton?
\item
  To what extent might \emph{Etëtung} help the people of Leweton
  advocate for local, national, and international environmental or
  climate action? What are some of the possibilities of drawing on
  \emph{Etëtung} as a cultural resource in this way? What are some
  limitations? Are there any risks involved for the community?
\item
  Can you think of any other music genres, especially near where you
  live, that carry environmental knowledge, or that play a role in
  advocating for environmental care or action? What roles are they
  playing, and how?
\item
  The video represents a collaboration between people from the Leweton
  community and ``outsider'' researchers. What might have been some
  advantages and challenges of such a collaboration for the community?
  What about for the researchers? How might the challenges be overcome?
\end{enumerate}

\hypertarget{recommended-reading}{%
\subsection*{Recommended Reading}\label{recommended-reading}}

\begin{hangparas}{15pt}{1}
  Dick, Thomas. 2014. ``Vanuatu Water Music and the Mwerlap Diaspora:
  Music, Migration, Tradition, and Tourism''. \emph{AlterNative: An
  International Journal of Indigenous Peoples} 10(4): 392--407.

  Grant, Catherine. 2019. ``Climate Justice and Cultural Sustainability:
  The Case of Etëtung (Vanuatu Women's Water Music).'' \emph{The Asia
  Pacific Journal of Anthropology} 20(1): 42-56.

  Hayward, P. 2014. ``Sounding the Aquapelago: The Cultural-Environmental
  Context of Ni-Vanuatu Women's Liquid Percussion Performance.''
  \emph{Perfect Beat} 15(2): 113.
\end{hangparas}
\vspace{16pt} %5mm vertical space

\hypertarget{digital-resources}{%
\subsection*{Digital Resources}\label{digital-resources}}

\begin{hangparas}{15pt}{1}
  Cole, Tim (director), Sur, Sandy (story), Wavales, Hilda (story),
  Leweton Cultural Group (performers). 2014. \emph{Vanuatu Women's Water
  Music} {[}film{]}. Port Vila: Further Arts. Available on Kanopy
  database.
\end{hangparas}
\vspace{16pt} %5mm vertical space

\hypertarget{works-cited}{%
\section*{Works Cited}\label{works-cited}}

\begin{hangparas}{15pt}{1}
  Dick, Thomas. 2014. ``Vanuatu Water Music and the Mwerlap Diaspora:
  Music, Migration, Tradition, and Tourism''. \emph{AlterNative: An
  International Journal of Indigenous Peoples} 10(4): 392--407.

  Feld, Steven. 2012. \emph{Sound and Sentiment}. Philadelphia: University
  of Pennsylvania Press.

  Grant, Catherine. 2019. ``Climate Justice and Cultural Sustainability:
  The Case of Etëtung (Vanuatu Women's Water Music).'' \emph{The Asia
  Pacific Journal of Anthropology} 20(1): 42-56.

  Hayward, Philip. 2014. ``Sounding the Aquapelago: The
  Cultural-Env\-iron\-men\-tal Context of ni-Vanuatu Women's Liquid Percussion
  Performance.'' \emph{Perfect Beat} 15(2): 113-127.

  \sloppy Intergovernmental Panel on Climate Change. 2021. ``AR6 Climate Change
  2021: The Physical Science Basis.'' \emph{Intergovernmental Panel on
  Climate Change}. \url{https://www.ipcc.ch/report/ar6/wg1/}.

  United Nations Children's Fund. 2016. ``Cyclone Pam: One Year On.''
  \emph{Relief Web}. 12 March.
  \url{https://reliefweb.int/report/vanuatu/unicef-fast-facts-cyclone-pam-one-year-march-2016}.

  World Vision. 2020. ``Cyclone Harold Leaves 160,000 Homeless: CAT II
  Disaster Declared.'' \emph{World Vision}. 11 April.
  \url{https://www.worldvision.com.au/media-centre/resource/cyclone-harold-leaves-160-000-homeless-cat-ii-disaster-declared}.
\end{hangparas}

\end{document}