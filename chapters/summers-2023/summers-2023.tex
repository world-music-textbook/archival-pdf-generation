\documentclass[twoside]{article}
\usepackage{mathpazo}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{xurl}
\usepackage[colorlinks]{hyperref}
\hypersetup{
  colorlinks,
  urlcolor=blue,
  linkcolor=black
}
\usepackage{caption} % unnumbered video examples
% \usepackage{authblk} % author affiliations
\usepackage{hanging} % hanging indents

% headers
\usepackage{fancyhdr}
\renewcommand{\headrulewidth}{0pt}

% first page footer
\fancypagestyle{infofooter}{%
  \fancyhf{}
  \renewcommand\headrulewidth{0pt}
  \fancyfoot[L]{\sffamily\small 
    WORLD MUSIC TEXTBOOK, ISSN: 2767-4215; \copyright~2023, CC-BY-NC-ND\\
    https://doi.org/10.25035/wmt.2023.004}
}
\thispagestyle{infofooter} % remove header from first page
\pagestyle{fancy}     % add headers in other pages

% normal headers
\fancyhead[LO]{\sffamily\small \textbf{\thepage} \quad World Music Textbook}
\fancyhead[RE]{\sffamily\small Summers: Video Game Music \quad \textbf{\thepage}}
\fancyfoot{}

% flush left title
\makeatletter
\renewcommand{\maketitle}{\bgroup\setlength{\parindent}{0pt}
\begin{flushleft}
  \vspace*{3\baselineskip}
  \huge{\textbf{\@title}}

  \medskip
  
  \large{\@author}
\end{flushleft}\egroup
}
\makeatother

% for keywords and abstract
\providecommand{\abstracttext}[1]
{
  \noindent
  \textbf{Abstract:} #1
}

\providecommand{\keywords}[1]
{
  \newline
  \textbf{Keywords:} #1
}

% for link
\providecommand{\wmturl}{\href{https://worldmusictextbook.org/summers-2023}{https://worldmusictextbook.org/summers-2023}}
\providecommand{\wmturltext}{
  \noindent\emph{This chapter is available online at \wmturl.}
}
\providecommand{\wmturlcaption}{
  Visit \href{https://worldmusictextbook.org/summers-2023}{the website} to view video examples.
}

% metadata
\title{Video Game Music}
\author{Tim Summers | Royal Holloway University of London}
% \affil{}

\date{}

% document
\begin{document}
\suppressfloats % prevent float above title
\maketitle

\abstracttext{This chapter considers how video game music developed and cultural and ethical challenges in creating gameworlds with music.}
\keywords{identity, media, music technology}

\smallskip

\wmturltext

\medskip

\noindent\hfil\rule{0.5\textwidth}{0.4pt}\hfil

\bigskip

\hypertarget{introduction}{%
\section*{Introduction}\label{introduction}}

\hypertarget{early-electronic-game-music}{%
\subsection*{Early Electronic Game
Music}\label{early-electronic-game-music}}

It is difficult to pinpoint the exact moment when video game music
began. Maybe it started with a room-sized computer at Manchester
University in 1951, where a computerized game of \emph{Draughts}
finished with a computer-produced melody of ``God Save the King.''
Perhaps it was with the `beeps' of the arcade hit \emph{Pong} (1972).
However, by 1977, arcade games like \emph{Circus} undoubtedly played
music.

Initially, game music was created through ``sound on chips'': music was
stored as instructions sent to sound chips that played the music.
Usually, these chips would only be able to play a few notes
simultaneously, and they had specific timbres (waveforms) and ranges
that they could produce. This method of making sound was so clearly
electronic that it gave the music a distinctively artificial quality.
With the technological development of sound hardware and increased
storage space, it became possible to include more pre-recorded sound in
games. Games could now play back recordings rather than make the music
in real-time on chips. With this change, any kind of recording---such as
orchestral, rock, or electronica recordings---could be incorporated into
games. The mid-to-late 1990s saw an uptick in recorded music in games,
primarily because of the development of the CD-ROM format that increased
storage.

Video games have long looked to their sibling media of film and
television for inspiration. With the increase of recorded music in
games, orchestral recordings---like those of Hollywood films---started
to appear more in games. \emph{The Lost World: Jurassic Park} (1997),
the first-released game with a newly-com\-posed orchestral score, was a
film tie-in to the Jurassic Park franchise. Modern games routinely
incorporate orchestral music, but it would be a mistake to characterize
game music history as simply a journey from computer chip instructions
to previously-recorded soundtracks.

\hypertarget{interactivity-and-game-music}{%
\subsection*{Interactivity and Game
Music}\label{interactivity-and-game-music}}

Games differ from film and television because they are interactive. That
interactivity can take many forms, but it usually means that timing and
outcome are variable. Therefore, composers must write music that can
adapt depending on the nature and timing of in-game events. Kasey
Collins and others call this ``dynamic music'' (2008). There are many
approaches to dynamic music, but it usually involves writing music in
discrete chunks that can then be looped or re-ordered as required by the
gameplay. It's a bit like musical Lego, which is why Elizabeth
Medina-Gray calls game music ``modular music'' (Medina-Gray 2016).
Composers can also write musical chunks that are designed to be stacked
on top of each other so that the music can respond to the gameplay
through adding or subtracting layers. Both the nature of the musical
system and the musical material will affect how the player experiences
music in the game.

Music that reacts to you while playing a game can be thrilling. When we
listen to most music, we cannot affect how the music plays out unless we
rewind, skip ahead, or alter the playback speed. In games, we can. These
musical reactions point to the importance of interactivity in gaming and
raise questions about how players engage with game music. Players and
scholars often assert that music contributes to the phenomenon of
engagement with games, using the term ``immersion.'' Yet, there is no
agreement on the definition of this widely-used word---whether it means
feeling like you are present in the space of the gameworld's reality (as
in believing you are no longer in your living room, but have been
transported to, for instance, another planet), simply being deeply
involved and engaged with playing the game, or some combination of the
two (Calleja 2011). While music contributes to both meanings, it can be
more confusing than useful, so it is worth being mindful about if, and
how, we use it (see Van Elferen 2016).

\hypertarget{playing-music-ludomusicology}{%
\subsection*{"Playing" Music:
Ludomusicology}\label{playing-music-ludomusicology}}

Ludomusicology is a term that entered musical circles in the 2010s to
refer to the study of music and play. While several languages use the
word ``play'' both for musical performance and for taking part in games,
scholarship has only recently begun exploring the connection between the
two. While many ludomusicological discussions concern video game music
(since digital games provide such a clear and fruitful avenue for
examining this connection), ludomusicology is by no means limited to
game music (Moseley 2016). Indeed, many game music scholars emphasize
the connections between the music of games and in other media. We often
seek to ask both what music ``does'' in games, and what games can tell
us about music outside games.

Fan engagement with video game music has proven to be important for
creating musical communities outside of the games themselves. Chiptune,
for example, involves making music with the sounds of old consoles and
sound chips. Many of these musicians argue that game music abandoned its
distinctive sonic quality when recorded music became dominant in games:
we have lost the specific artificiality and methods of composing that
were unique to sound on chips (McAlpine 2019). Chiptune fans choose to
celebrate these older technologies and sounds while finding new methods
to make music with the old chips. Old technologies become resources for
self-expression, tools for reacting against the accepted wisdom of
``newer is better.'' Whether through the vast number of covers of game
music on sites like YouTube and OverClocked Remix, orchestral concerts
of game music (like \emph{Video Games Live}), or the chiptune community,
many people clearly demonstrate how game music is meaningful to them.

To talk about ``game music'' is to discuss music that can involve nearly
any kind of musical style. There are few absolutes about what game music
is or is not. It can be dynamic music, but some of the most popular game
music is not interactive. It can be chiptune, but that is only a subset
of game music. It can be composed specifically for a game, but games
also meaningfully use pre-existing music, too. Games are a way to
examine, discuss, and better understand musical experiences and
meanings. I offer two case studies to explore how that might work in
practice, but more than anything, I encourage you to undertake your own
explorations and adventures in game music and make your own discoveries.
In short: go play!

\hypertarget{two-case-studies}{%
\section*{Two Case Studies}\label{two-case-studies}}

Gamers may play the racing game \emph{Gran Turismo} without being
especially conscious of the game's Japanese origin, just as the
\emph{Grand Theft Auto} games---set in the United States---hardly
trumpet their Scottish origins. In contrast, part of the appeal of
puzzle games like \emph{Katamari Damacy} are their specifically Japanese
cultural identity. As William Gibbons has explored (2021), music can
help either to accentuate or obscure the cultural origin of the game. A
game may use culturally specific musical aesthetics and verbal
languages, opt for more generic stylistic features, or tailor the music
to the region in which the game is released.

Game music intersects with cultural representation and identity in
complex ways. In this exploration, we will consider two aspects of game
music and culture---first, the treatment of musical cultures in games,
and secondly, the musical cultures of video games. Video games often
construct virtual worlds for us to spend time in. Sometimes these may be
fantastic inventions, while other times they may be closer to our
reality. In games that involve exploration, the creation of these spaces
as compelling and believable is a high priority for the developers.
Unsurprisingly, music plays an important role in building these virtual
worlds and cultures---whether that music comes from depictions of
music-making by the inhabitants of the world (what scholars would call
\emph{diegetic} music), or it accompanies the playing without a specific
visual source identified in the narrative world (\emph{non-diegetic}
music).

\hypertarget{the-legend-of-zelda-ocarina-of-time-1998}{%
\subsection*{The Legend of Zelda: Ocarina of Time
(1998)}\label{the-legend-of-zelda-ocarina-of-time-1998}}

An instructive example is \emph{The} \emph{Legend of Zelda: Ocarina of
Time}, an action-adven\-ture game. In this fantasy game, the player's
character (traditionally referred to as `Link') explores the universe of
Hyrule and the varied cultures within it. The music's composer Koji
Kondo draws on a huge variety of musical styles and uses them to
represent these fictional cultures. To do so, as is the case with many
games, Kondo sometimes borrows from real-world musics. For example, the
forest uses Javanese angklungs, a desert dungeon includes instruments
and scales from Carnatic South Indian classical music, the ranch uses
American country music tropes, the market town alludes to European
renaissance music, and the mountain has instrumentation that mimics
instruments from Brazil, West Africa, India, and others.

However, this strategy raises questions. Is it appropriate for Kondo to
use instrumentation, techniques, and materials from specific musical
traditions, separating them from their cultural meanings? Is it
acceptable to blend different traditions together and redeploy them in
service of creating a fictional, fantasy world? It is particularly
problematic when these musical traditions have long been appropriated
and stereotyped in moments of racist cultural violence to accompany
stage and screen fictions.

On the one hand, we might argue that Kondo is clearly not attempting to
represent the cultures and musics he draws on---after all, these are
fantastic realities that blend multiple traditions together. They may
not count as misrepresentation. On the other hand, we can argue that
Kondo should not be superficially exploiting music belonging to other
cultures and disregarding cultural meanings, especially when the
cultural owners are not engaged in the creative process or able to
respond on an equal platform. Kondo and Nintendo are clearly aware of
musical sensitivities: in the earliest-made copies of the game, a piece
of music for the Fire Temple borrowed a sample of a sung Islamic call to
prayer, though later versions removed this sample.

Just as real-world places have musical traditions, so do places in the
virtual world---even if they are fictional and the result of blended,
borrowed musics. When we engage with the virtual world of Hyrule, we
encounter these cultural sounds as we musically explore the world.

Of course, there are many other examples of cultural representation and
engagement in games that contrast with that of \emph{Ocarina of Time}.
Examples include:

\begin{itemize}
\item
  Games that specifically represent a variety of real-world cultures,
  such as those from later in the \emph{Civilization} series that
  situate Western classical music as a universal neutral against which
  local musical traditions are contrasted, or how \emph{Pokémon Sun and
  Moon} draw on Hawaiian music (see Gunn 2022).
\item
  Games that use music to depict particular specific cultures
  extensively---as in \emph{Grand Theft Auto: San Andreas}, which puts
  the player in the shoes of an African-American (anti-) hero on the
  United States' West Coast in the 1990s (see Miller 2012).
\item
  Games that obviously mismatch the setting and musical traditions from
  which they draw. For example, \emph{Alone in the Dark} (2008 version)
  is set in modern New York but has a soundtrack that uses the
  distinctive timbres of the Bulgarian State Television Female Vocal
  Choir.
\item
  Games that use historical (mis)representation to convey their
  meanings, for example, the ahistorical medievalism in fantasy games
  like \emph{Assassin's Creed} and \emph{Stronghold 3} (see Lind 2023),
  or the ``alternate history'\,'' of Nazi-made 1960s pop songs in
  \emph{Wolfenstein: The New Order} (see Krishnaswami 2020)\emph{.}
\end{itemize}

\hypertarget{undertale-2015-and-deltarune-2018}{%
\subsection*{\texorpdfstring{\emph{Undertale} (2015) and
\emph{Deltarune}
(2018--)}{Undertale (2015) and Deltarune (2018--)}}\label{undertale-2015-and-deltarune-2018}}

Video games have themselves become an important site of musical cultural
activity. Nowhere is this clearer than in games that draw explicitly on
the musical histories of games and those that have prompted considerable
musical activity outside the game itself. Game music has provided new
opportunities for what musicologist Christopher Small would call
``musicking'' (1998), meaning engaging and interacting with music in a
variety of ways, not just composing or performing music.

\emph{Undertale} (2015) and its spiritual sequel, \emph{Deltarune}
(2018) are role-playing games (RPGs) made in the tradition of Japanese
RPGs of the 1980s and 1990s. They were both created by American
developer and musician Toby Fox, who also wrote the score for the games.
These games make use of ``retro'' pixel art aesthetics and musical
scores that reference older video game music and its related
technologies.

Though these games are twenty-first century creations, they choose to
celebrate the older style of more limited, less realistic, supposedly
``lower quality'' music technologies. For instance, the opening of
\emph{Undertale} is closely modelled on the opening of the Japanese RPG
\emph{Mother} (1989) and uses the limitations and chiptune sounds of the
console originally used to play \emph{Mother}. Both \emph{Undertale} and
\emph{Deltarune} draw upon a wide variety of musical references and vary
the timbres and styles from across video game history. These games have
found widespread appeal as love letters to older styles. This appeal
extends beyond nostalgia as many fans of \emph{Undertale} and
\emph{Deltarune} were born well after those technologies were replaced.

The music of \emph{Undertale} and \emph{Deltarune} have provided avenues
for numerous musicking activities for fans. For example,
\emph{Undertale} and \emph{Deltarune} use highly thematic scores,
allowing fans to trace the replication and development of the materials
across the games (VGMusicologist, 2015). Similarly, YouTube and Spotify
are filled with covers and adaptations of the music. We can
\href{https://undertalelive.com/}{go to concerts} of the games'
music,
\href{https://www.fangamer.com/collections/undertale/type_music}{listen
to it on records},
\href{https://materia.store/products/undertale-piano-collections-sheet-music-book}{buy
official sheet music}, or find transcriptions that fans have created
(Ahmaykmewsik 2016).

\hypertarget{conclusion}{%
\section*{Conclusion}\label{conclusion}}

\emph{Undertale} and \emph{Deltarune} are only two examples of the
widespread culture of game music fandom. Game designers are themselves
active in creating musical cultures, as the historical references in
\emph{Undertale} and \emph{Deltarune's} soundtracks make clear. There
are many ways in which music and video games interact outside of the
game, including:

\begin{itemize}
\item
  Concerts of video game music at dedicated events like
  \href{https://www.videogameslive.com/}{\emph{Video Games Live}} or
  \href{https://www.gameconcerts.com/en/concerts/final-symphony/}{\emph{Final
  Symphony}}.
\item
  \sloppy Dialogue between games and other musical and theatrical traditions,
  such as the staging of Mozart's
  \emph{\href{https://www.pacificoperaproject.com/production-history/the-magic-flute/march-2019}{The
  Magic Flute as a Super Mario Bros}.} production (Pacific Opera
  Project), Tan Dun's trombone concerto
  \href{https://issuu.com/scoresondemand/docs/three_muses_in_video_game_61915}{``Three
  Muses in Video Game,''} and the
  \href{https://www.j25musical.jp/en/stage/647}{adaption of Final
  Fantasy X to a kabuki performance}.
\item
  The role of games in music education, whether in music from
  \emph{Mother} appearing in state-approved school textbooks, or the
  references to game music in A-Level and GCSE (General Certificate of
  Secondary Education) syllabi in the United Kingdom.
\end{itemize}

I have sought to present several of the complex ways that video game
music is bound up with questions of culture. It is important to consider
questions of representation and how players engage with musical cultures
in games. We can see how games themselves host musical cultures both
within and beyond the games. At its core, the argument here is that game
music is meaningful to players and it is culturally valuable and
powerful. It is worth taking seriously, even while we have fun!

\hypertarget{discussion-questions}{%
\section*{Discussion Questions}\label{discussion-questions}}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\item
  How do the parameters of a particular music technology affect the
  specific ways in which music is composed? What are the benefits and
  risks of focusing specifically on the limits that musicians faced (as
  is so often the case in discussions of chiptune)? Is newer, more
  powerful, easier to use technology always better?
\item
  What does it mean for music to be interactive? How do different types
  of interactivities produce different results for players? Can music be
  too interactive?
\item
  In what other contexts beyond video games might a broad
  ludomusicological approach---exploring music and play---be helpful?
\item
  What factors are at play when a composer draws on real-world musical
  traditions (to which they do not belong) in the depiction of obviously
  fantastic worlds? What determines the appropriateness of this
  activity?
\item
  How does engaging with virtual fictional cultures in games compare and
  contrast with engaging with real-world cultures? What implications
  might this have?
\item
  What constitutes the musical cultures of video games? How have games
  and the practices that surround them articulated, enabled, and
  celebrated this culture?
\end{enumerate}

\hypertarget{recommended-reading}{%
\section*{Recommended Reading}\label{recommended-reading}}

\begin{hangparas}{15pt}{1}
Gibbons, William. 2021. "Open Worlds: Globalization, Localization and
Video Game Music. " In \emph{The Cambridge Companion to Video Game
Music}, edited by Melanie Fritsch \& Tim Summers, 359--375. Cambridge:
Cambridge University Press.

Gunn, Milly. 2022. "The Soundscape of Alola: Exploring the Use of
Hawaiian Musical Tropes and Motifs in the World of Pokémon Sun and
Moon." \emph{Journal of Sound and Music in Games} 3(2--3):59--76.

Krishnaswami, Ravi. 2020. "Playing Songwriter: Creating Songs for the
Fictional Worlds of Video Games." \emph{Journal of Sound and Music in
Games} 1(2):68--83.

Lind, Stephanie. 2023. \emph{Authenticity in the Music of Video Games.}
London: Lexington.

Miller, Kiri. 2012. \emph{Playing Along: Digital Games, YouTube, and
Virtual Performance}. New York: Oxford University Press.
\end{hangparas}

\hypertarget{works-cited}{%
\section*{Works Cited}\label{works-cited}}

\begin{hangparas}{15pt}{1}

Ahmaykmewsik. 2016.
\href{https://www.reddit.com/r/UndertaleMusic/comments/56ecnh/the_unofficial_complete_transcription_project/}{"Undertale:
The Unofficial Complete Transcription -- Unfinished Pre-release
Edition."} Accessed August 16, 2023.

Calleja, Gordon. 2011. \emph{In-Game: From Immersion to Incorporation.}
Cambridge, MA: The MIT Press.

Collins, Kasey. 2008. \emph{Game Sound: An Introduction to the History,
Theory and Practice of Video Game Music and Sound Design.} Cambridge,
MA: The MIT Press.

Donnelly, K.J., William Gibbons and Neil Lerner, eds. 2014. \emph{Video
Game Music: Studying Play.} New York: Routledge.

\sloppy Fernández-Cortés, Juan Pablo. 2021. "Ludomusicology: Normalizing the
Study of Video Game Music." Translated by Karen M. Cook. \emph{Journal
of Sound and Music in Games} 2(4): 13--35.

Fritsch, Melanie and Tim Summers, eds. 2021. \emph{The Cambridge
Companion to Video Game Music.} Cambridge: Cambridge University Press.

McAlpine, Kenneth B. 2019. \emph{Bits and Pieces: A History of
Chiptunes.} New York: Oxford University Press.

Medina-Gray, Elizabeth. 2016. "Modularity in Video Game Music, " in
\emph{Ludomusicology: Approaches to Video Game Music,} edited by Michiel
Kamp, Tim Summers and Mark Sweeney, 53--72. Sheffield: Equinox.

\emph{Reformat the Planet} {[}Documentary{]}. 2009. Directed by Paul
Owens. 2 Player Productions.

Moseley, Roger. 2016. \emph{Keys to Play: Music as a Ludic Medium from
Apollo to Nintendo}. Oakland, CA: University of California Press.

Small, Christopher. 1998. \emph{Musicking: The Meanings of Performing
and Listening}. Hanover: University Press of New England.

Van Elferen, Isabella. 2016. "Analyzing Game Musical Immersion: The ALI
Model," in \emph{Ludomusicology: Approaches to Video Game Music}, edited
byMichiel Kamp, Tim Summers and Mark Sweeney, 32--52. Sheffield:
Equinox.

VGMusicologist. 2015.
\href{https://ocremix.org/community/topic/41969-undertale-soundtrackanalysis-huge-spoilers}{"Undertale
Soundtrack Analysis."} OCRemix, 22 October 2015. Accessed August 16,
2023.

\end{hangparas}
\end{document}