\documentclass[twoside]{article}
\usepackage{mathpazo}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{xurl}
\usepackage[colorlinks]{hyperref}
\hypersetup{
  colorlinks,
  urlcolor=blue,
  linkcolor=black
}
\usepackage{caption} % unnumbered video examples
% \usepackage{authblk} % author affiliations
\usepackage{hanging} % hanging indents

% headers
\usepackage{fancyhdr}
\renewcommand{\headrulewidth}{0pt}

% first page footer
\fancypagestyle{infofooter}{%
  \fancyhf{}
  \renewcommand\headrulewidth{0pt}
  \fancyfoot[L]{\sffamily\small 
    WORLD MUSIC TEXTBOOK, ISSN: 2767-4215; \copyright~2024, CC-BY-NC-ND\\
    https://doi.org/10.25035/wmt.2024.002}
}
\thispagestyle{infofooter} % remove header from first page
\pagestyle{fancy}     % add headers in other pages

% normal headers
\fancyhead[LO]{\sffamily\small \textbf{\thepage} \quad World Music Textbook}
\fancyhead[RE]{\sffamily\small Alexander: The Study of Music and Gender \quad \textbf{\thepage}}
\fancyfoot{}

% flush left title
\makeatletter
\renewcommand{\maketitle}{\bgroup\setlength{\parindent}{0pt}
\begin{flushleft}
  \vspace*{3\baselineskip}
  \huge{\textbf{\@title}}

  \medskip
  
  \large{\@author}
\end{flushleft}\egroup
}
\makeatother

% for keywords and abstract
\providecommand{\abstracttext}[1]
{
  \noindent
  \textbf{Abstract:} #1
}

\providecommand{\keywords}[1]
{
  \newline
  \textbf{Keywords:} #1
}

% for link
\providecommand{\wmturl}{\href{https://worldmusictextbook.org/alexander-2024b}{https://worldmusictextbook.org/alexan der-2024b}}
\providecommand{\wmturltext}{
  \noindent\emph{The online version of this chapter is available at \wmturl.}
}

% metadata
\title{The Study of Music and Gender}
\author{Kathryn Alexander | University of Arizona}
% \affil{}

\date{}

% document
\begin{document}
\suppressfloats % prevent float above title
\maketitle

\abstracttext{This article provides an overview of critical issues and scholarly developments pertaining to music with gender.}
\keywords{music notes, ethnomusicology}

\smallskip

\wmturltext

\medskip

\noindent\hfil\rule{0.5\textwidth}{0.4pt}\hfil

\bigskip

\begin{sloppypar}
\noindent\textbf{Gender:} a state of experiencing (mis-)alignment with socially
construc\-ted roles, behaviors, expressions, and identities attached to
categories of bodies delineated by a culture's unique criteria. Gender
may hinge on visible anatomy (sex), embodied practices, or other
characteristics.
\end{sloppypar}

\hypertarget{gender-identity-and-music-research}{%
\section*{Gender Identity and Music
Research}\label{gender-identity-and-music-research}}

Gender is a category of experience that is both uniquely personal and
heavily impacted by cultural contexts. It is a way of organizing human
beings and bodies into alliances formed around the meanings, social
roles, and characteristics (anatomical, hormonal, chromosomal, and
behavioral) that attach to and demarcate genders within a particular
culture. Gender is one of those slippery, elusive frames of embodied
cultural knowledge. The ways we define gender, such as clothing,
behaviors, or other means are not innately gendered: We give these
symbols gendered meanings within our culture's norms and belief systems.
Though its meaning is thus culturally constructed, gender has profound
cultural relevance. How gender is defined thus also varies between
cultures.

Gender identity is the experience of gender and how a person makes their
own space within or outside the boundaries of a socially legible gender
category, such as ``male'' or ``female''; this may involve creating
alternate cultural spaces in which their gender identity can be
meaningful. In contrast, sex is anatomy, hormones, and chromosomes.
Gender may be based around sex characteristics; however, gender identity
and sex need not map onto each other. Since the 1970s, gender-focused
activists have worked through scholarship, legislation, and community
action to dissolve these linkages. For example, my own work on LGBTQ
country western cultural forms like dance and rodeo explores how these
enable a greater diversity of queer people to create meaningful
identities and empowered communities (see Alexander 2019).

\hypertarget{second-wave-feminism-and-impacts-on-research-methods}{%
\section*{\raggedright Second Wave Feminism and Impacts on Research
Methods}\label{second-wave-feminism-and-impacts-on-research-methods}}

\raggedbottom
In the latter twentieth century, music scholars began devoting sustained
attention to the ways that gender impacts musical phenomena and
communities. Beginning in the 1970s, music scholars took inspiration
from the social justice activism of Second Wave feminists, whose primary
goals included greater equity for women in areas such as reproductive
rights and pay equity. At the same time, those marginalized in America's
mainstream feminist movement, such as lesbian activists and feminists of
color from many ethnic backgrounds, advocated for their own communities'
rights alongside women's rights. These women faced homophobia and racism
in addition to sexism and misogyny. Drawing on these new currents,
ethnomusicologists initially sought to reveal how women's experiences
and roles as musicians differed from men's in various cultures. In the
1980s, North American ethnomusicologists increasingly paid attention to
gender (Sugarman 2019:71), and ethnomusicologists also began to
recognize that male informants had long framed (male)
ethnomusicologists' understandings of musical cultures. They observed
that this resulted in systemically gendered perspectives (Nettl
1983:334). The recognition that a researcher's gender impacts their
research offered opportunities to fill in previously undocumented
aspects of gender within music cultures, such as musics performed only
by women for women, as is the case of healing rituals in Islamic eastern
Africa and the Arabian Gulf, such as Zār ceremonies.

Female ethnomusicologists initially addressed this gap by refocusing on
women's participation in musical cultures (Koskoff 1987; Moore 1988;
Abu-Lughod 1990; Doubleday {[}1988{]} 2006; Magrini 2003). The scope of
this work gradually broadened to consider the impacts of gender more
widely, and no longer limited itself to the experiences of women (Roy
2019; Thorn 2019; Spiller 2019). Researchers increasingly highlighted
the culturally constructed reality of genders and the systemic nature of
gendered experience. For example, Jeff Roy explores third gender
categories in India, known as \emph{hijra} (2019). Meanwhile, Henry
Spiller's investigation of cross-dressing performances in West Java
examines how these performances reinscribe normative gender roles
(2019). Scholars also considered intersectional experiences that include
gender, producing a growing body of work that looks at how racial and
gender identities together impact musical participation (Gaunt 2006;
Keyes 2006; Wong 2006; Hayes 2010). In the 2000s, scholars produced a
steady stream of edited collections encompassing a wide range of topics
and issues surrounding women's participation in musical cultures
(Moisala and Diamond 2000; Magowan and Wrazen 2013; Bernstein 2014;
Koskoff 2014).

\hypertarget{reflexivity-and-the-role-of-the-researcher}{%
\section*{Reflexivity and the Role of the
Researcher}\label{reflexivity-and-the-role-of-the-researcher}}

Ethnography is a key method for generating research data in fields such
as anthropology and ethnomusicology. Until the latter
20\textsuperscript{th} century, ``good'' ethnographic research was
marked by its presumed objectivity---meaning that the researcher was
impartial in gathering data, something which is valued in the empirical
sciences. Social sciences like anthropology attempted to replicate this
idea of objectivity as much as possible. One of the most sustained
impacts of a feminist approach is the rise of ``reflexivity,'' meaning
the ability to interrogate one's own subject position, biases, and
motivations. Early twentieth century research attempted to remain
objective and did not analyze the role of the researcher; however, later
scholars questioned whether such research was as impartial as it claimed
or whether it instead masked biases rather than acknowledging them.
Reflexivity arose in the 1980s and became an expected praxis by the
early 2000s (Barz and Cooley 2008:13). Through reflexivity, the
fieldworker, or the researcher engaging directly with communities,
explicitly recognizes that their body's identities have cultural
meaning. Further, they recognize that these identities impact the
community they work in and consequently influence the research they
produce. In short, the researching self is always a participant rather
than someone separate or apart from the research. Researchers also began
to recognize their place as agents of change who could impact the
subjects of their research, and how research might in turn impact them
(Hahn 2007; Wong 2008, 2019). To capture this complexity, practitioners
of autoethnography---an ethnography that centrally includes the
researcher's experiences as a data source---work to thoroughly identify
and unpack one's place in a research site. This dissolves the idea that
fieldworkers are neutral and interchangeable: Who we are matters for our
work, methods, outcomes, and even safety. Autoethnography is a key
method through which gender has become more accessibly documented,
defined, discussed, and dissolved (Alexander 2019; Castro 2019).

Gender-focused ethnomusicologists broadly acknowledge the real impacts
of gender, both as an analytical lens and embodied identity. Gender
impacts epistemology (what we know), methodology (how we come to know
it), and ontology (how we make sense of what we know). With these
acknowledgements, once-canonical research methods were recognized as
dependent on a scholar's privileged gender identity rather than
universally available to anyone. Fieldworkers increasingly acknowledge
their gender---what they feel and how they are perceived by
others---since ethnomusicology began to acknowledge its colonial
background and began to work towards more ethical research practices and
scholarship. For example, ethnomusicologist Veronica Doubleday
demonstrates responsiveness to her field site and interlocutors by
choosing to veil while working with women in Afghanistan ({[}1988{]}
2006). Similarly, Carol Babiracki directly addresses the impact of her
gender identity, assigned gender roles, and interlocutors' expectations
of her based on her gender during her field research in India and on her
subsequent representation of that research in writing (1997/2008). She
ultimately finds gender neutrality impossible, a conclusion that challenges
the belief held by earlier generations of predominantly white, male
scholars that they were neutral observers. Nicole Beaudry likewise
chooses to downplay her ``feminine identity'' during her field research
but questions this decision, recognizing that her belief in equity
between men and women---which perhaps undergirded her enforced gender
neutrality---is not shared by her interlocutors. This leads her to
consider a series of ethical quandaries. Rhetorically, she asks, ``is
gender a matter than can ever be left aside?'' (1997:82; 2008:243).
Other authors, like Timothy Rice, note the ways they are distanced from
a tradition by their male gender identity, and how their gender is
(dis)empowered in particular field research contexts (1997:107;
2008:48). This marks a significant change from earlier generations'
assumptions that the male experience was universal, or at least
sufficient to understand a culture's music making.

\begin{sloppypar}
Women's music is still considered in ways that reflect contemporary
feminist scholars' interest in highlighting cultural specifics rather
than cross-cul\-tural universals, and showing how large-scale structures
like laws or gender norms impact women's lives. Trying to understand a
music within its own cultural norms pushes against the ``righteous
indignation'' (Sarkissian 1999) felt by scholars projecting Western
feminism's empowering, even anti-patriarchal ethos, onto women's
experiences in other cultural contexts. Lila Abu-Lughod (1986, 1993) and
Virginia Danielson (1997) laid early groundwork to decenter Western
gender expectations of Arab women's music-making. Similarly, Jessica
Roda (2024) identifies the culturally legible protections of women's
agency in some North American Orthodox Jewish communities that, from a
mainstream feminist perspective, are overshadowed by patriarchal
structures. Structural gender systems and women's negotiations of them
are also considered in Central Asia by Tanya Merchant (2015) and Razia
Sultanova (2022). Merchant offers a comprehensive view of Uzbek female
musicians' landscape of musicality, while Sultanova considers the
impacts of the Taliban on women's music-making.
\end{sloppypar}

\hypertarget{queer-theory-and-conclusions}{%
\section*{Queer Theory and
Conclusions}\label{queer-theory-and-conclusions}}

The impacts of queer theory on gender-focused scholarship is growing.
Gregory Barz and William Cheng's collection \emph{Queering the Field:
Sounding Out Ethnomusicology} includes gender-diverse scholars.
Throughout the collection, authors consider gender in myriad forms,
including the impacts of gender identity on their production and
presentation of knowledge, engagement with field sites and subjects, and
constructions of the scholarly self (2019). For example, I use
autoethnography and reflexivity to reflect on how my gender identity was
frequently misidentified in Cape Breton's Scottish social dance halls.
This gender ambiguity presented challenges for inclusion in the dances,
which have clearly defined roles for men and women (2019). A body
between the binary was not so easily understood. Queer theorists' work
helps interpret these moments of ambiguity; it highlights how identity
is mapped onto bodies, and how both bodies and identity are the product
of cultural meaning-making.

In summary, gender-focused ethnomusicologists reveal the ways that
gender functions in a wide array of musical cultures, and in the field
of ethnomusicology itself. Though the ``different social and cultural
understandings of gender and music'' have not allowed for easy
cross-cultural comparison or broader theorizing (Cusick 2014), the
enlisting of queer theoretical models to decenter and deconstruct
normative and universal ideas of gender, the rise of autoethnography,
and an awareness of individual and structural biases may lessen the
drive for universals. Instead, these models offer opportunities to
decenter Western cultural gender norms.

\hypertarget{discussion-questions}{%
\section*{Discussion Questions}\label{discussion-questions}}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\item
  How does approaching gender as a cultural construction, rather than a
  biological given, impact your understanding of the world around you?
\item
  In what ways might a focus on gender aid your ability to understand
  new musical contexts?
\item
  How do you react to writing in which the author's bias or perspective
  is clear? Do you think it is possible for a scholar to have true
  neutrality on a subject? Do you think reflexivity helps reduce bias in
  writing?
\end{enumerate}

\hypertarget{recommended-readingmedia}{%
\section*{Recommended Reading/Media}\label{recommended-readingmedia}}
\begin{hangparas}{15pt}{1}

\raggedright Butler, Judith. ``Judith Butler: Your Behavior Creates Your Gender \textbar{} Big Think,'' YouTube video, 3:00. Posted by ``Big Think,'' June 6, 2011. \url{https://www.youtube.com/watch?v=Bo7o2LYATDc}.

Spiller, Henry. 2014. ``Introduction: Music, Movement, and Masculinities.'' \emph{The World of Music (New Series)} 3(2):5-13.

Sugarman, Jane C. 1997. \emph{Engendering Song: Singing and Subjectivity
at Prespa Albanian Weddings}. Chicago: The University of Chicago Press.
\end{hangparas}

\vspace{16pt} %5mm vertical space
\hypertarget{works-cited}{%
\section*{Works Cited}\label{works-cited}}
\begin{hangparas}{15pt}{1}

Abu-Lughod, Lila. 1986. \emph{Veiled Sentiments: Honor and Poetry in a
Bedouin Society}. Berkeley: University of California Press.

---------. 1990. ``The Romance of Resistance: Tracing Transformations of Power
through Bedouin Women.'' \emph{American Ethnologist} 17(1):41-55.

Alexander, Kathryn. 2018. ``Politely Different: Queer Presence in
Country Dancing and Music.'' \emph{Yearbook for Traditional Music}
50:187-209.

---------. 2019. ``Straight to the Heart: Heteronormativity,
Flirtation, and Autoethnography at Home and Away.'' In \emph{Queering
the Field: Sounding Out Ethnomusicology}, edited by Gregory Barz and
William Cheng, 291-306. New York: Oxford University Press.

Barz, Gregory and William Cheng. 2019. \emph{Queering the Field:
Sounding Out Ethnomusicology}. New York: Oxford University Press.

Babiracki, Carol M. 1997. ``What's the Difference? Reflections on Gender
and Research in Village India. In \emph{Shadows in the Field: New
Perspectives for Fieldwork in Ethnomusicology}, first edition, edited by
Gregory Barz and Timothy J. Cooley, 121-138. New York: Oxford University
Press.

---------. 2008. ``What's the Difference? Reflections on Gender
and Research in Village India. In \emph{Shadows in the Field: New
Perspectives for Fieldwork in Ethnomusicology}, second edition, edited
by Gregory Barz and Timothy J. Cooley, 167-182. New York: Oxford
University Press.

Beaudry, Nicole. 1997. ``The Challenges of Human Relations in
Ethnographic Inquiry: Examples from Arctic and Subarctic Fieldwork.'' In
\emph{Shadows in the Field: New Perspectives for Fieldwork in
Ethnomusicology}, first edition, edited by Gregory Barz and Timothy J.
Cooley, 63-83. New York: Oxford University Press.

---------. 2008. The Challenges of Human Relations in
Ethnographic Inquiry: Examples from Arctic and Subarctic Fieldwork.'' In
\emph{Shadows in the Field: New Perspectives for Fieldwork in
Ethnomusicology}, second edition, edited by Gregory Barz and Timothy J.
Cooley, 224-245. New York: Oxford University Press.

Bernstein, Jane A., ed.~2004. \emph{Women's Voices across Musical
Worlds}. Boston: Northeastern University Press.

Crapazano, Vincent. 1973. \emph{The Hamadsha: A Study of Moroccan
Ethnopsychiatry}. Berkeley: University of California Press.

Cusick, Suzanne. 2014. ``Out in Left Field/Left Out of the Field:
Postmodern Scholarship, Feminist/Gender Studies, Musicology, and
Ethnomusicology, 1990-2005. In \emph{A Feminist Ethnomusicology:
Writings on Music and Gender}, edited by Ellen Koskoff, 168-179. Urbana:
University of Illinois Press.

Danielson, Virginia. 1997. \emph{``The Voice of Egypt'': Umm Kulthum,
Arabic Song, and Egyptian Society in the Twentieth Century}. Chicago:
The University of Chicago Press.

Doubleday, Veronica. {[}1988{]} 2006. \emph{Three Women of Herat: A
Memoir of Life, Love and Friendship in Afghanistan}. London: Tauris
Parke Paperbacks.

---------. 2006. ``The Frame Drum in the Middle East: Women,
Musical Instruments, and Power.'' In \emph{Ethnomusicology: A
Contemporary Reader}, edited by Jennifer C. Post, 109-133. New York:
Routledge.

---------. 2022.\emph{Three Women of Herat: Afghanistan
1973-1977}. London: Eland Publishing Ltd.

Gaunt, Kyra D. 2006. \emph{Games Black Girls Play: Learning the Ropes
from Double-Dutch to Hip-Hop}. New York: New York University Press.

Hahn, Tomie. 2007. \emph{Sensational Knowledge: Embodying Culture
Through Japanese Dance}. Middletown, CT: Wesleyan University Press.

Hayes, Eileen M. 2010. \emph{Songs in Black and Lavender: Race, Sexual
Politics, and Women's Music}. Urbana: University of Illinois Press.

Keyes, Cheryl L. 2006. ``Empowering Self, Making Choices, Creating
Spaces: Black Female Identity via Rap Music Performance.'' In
\emph{Ethnomusicology: A Contemporary Reader}, edited by Jennifer C.
Post, 97-108. New York: Routledge.

Koskoff, Ellen. 1987. \emph{Women and Music in Cross-Cultural
Perspective}. Urbana: University of Illinois Press.

Koskoff, Ellen, ed.~2014. \emph{A Feminist Ethnomusicology: Writings on
Music and Gender}. Urbana: University of Illinois Press.

Magowan, Fiona and Louise Wrazen, eds.~2013. \emph{Performing Gender,
Place, and Emotion in Music: Global Perspectives}. Cambridge: Cambridge
University Press.

Magrini, Tulia, ed.~2003. \emph{Music and Gender: Perspectives from the
Mediterranean.} Chicago: University of Chicago Press.

Merchant, Tanya. 2015. \emph{Women Musicians of Uzbekistan: From
Courtyard to Conservatory}. Urbana: University of Illinois Press.

Moisala, Pirkko and Beverley Diamond, eds.~2000. \emph{Music and
Gender}. Urbana: University of Illinois Press.

Rice, Timothy. 1997. ``Towards a Mediation of Field Methods and field
Experience in Ethnomusicology.'' In \emph{Shadows in the Field: New
Perspectives for Fieldwork in Ethnomusicology}, first edition, edited by
Gregory Barz and Timothy J. Cooley, 101-120. New York: Oxford University
Press.

---------. 2008. ``Towards a Mediation of Field Methods and field
Experience in Ethnomusicology.'' In \emph{Shadows in the Field: New
Perspectives for Fieldwork in Ethnomusicology}, second edition, edited
by Gregory Barz and Timothy J. Cooley, 42-61. New York: Oxford
University Press.

Roda, Jessica\emph{.} 2024. \emph{For~Women~and~Girls Only.~Reshaping
Jewish Orthodoxy Through~the~Arts~in the~Digital Age}. New York: New
York University Press.

Roy, Jeff. 2019. ``Con/Figuring Transgender-\emph{Hījṛā} Music and Dance
through Queer Ethnomusicological Filmmaking.'' In \emph{Queering the
Field: Sounding Out Ethnomusicology}, edited by Gregory Barz and William
Cheng, 163-184. New York: Oxford University Press.

Sarkissian, Margaret. 1999. ``Thoughts on the Study of Gender in
Ethnomusicology: A Pedagogical Perspective.'' \emph{Women \& Music}
3:1-17.

Spiller, Henry. 2019. ``Going through the Motions: Transgender
Performance in \emph{Topeng Cirebon} from North Java, Indonesia.'' In
\emph{Queering the Field: Sounding Out Ethnomusicology}, edited by
Gregory Barz and William Cheng, 198-216. New York: Oxford University
Press.

Sugarman, Jane. 2019. ``Theories of Gender and Sexuality.'' In
\emph{Theory for Ethnomusicology: Histories, Conversations, Insights},
second edition, edited by Harris M. Berger and Ruth M. Stone, 71-98. New
York: Routledge.

Sultanova, Razia. 2022. \emph{Afghanistan Dispossessed: Women, Culture
and the Taliban}. Yorkshire and Philadelphia: Pen and Sword History.

Wong, Deborah. 2006. ``Taiko and the Asian/American Body: Drums, `Rising
Sun,' and the Question of Gender.'' In \emph{Ethnomusicology: A
Contemporary Reader}, edited by Jennifer C. Post, 87-96. New York:
Routledge.

---------. 2008. ``Moving: From Performance to Performative
Ethnography and Back Again.'' In \emph{Shadows in the Field: New
Perspectives for Fieldwork in Ethnomusicology}, second edition, edited
by Gregory Barz and Timothy J. Cooley, 76-89. New York: Oxford
University Press.

\end{hangparas}
\end{document}