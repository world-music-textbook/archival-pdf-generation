\documentclass[twoside]{article}
\usepackage{mathpazo}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage[colorlinks]{hyperref}
\hypersetup{
  colorlinks,
  urlcolor=blue,
  linkcolor=black
}
\usepackage{caption} % unnumbered video examples
% \usepackage{authblk} % author affiliations
\usepackage{hanging} % hanging indents

% headers
\usepackage{fancyhdr}
\renewcommand{\headrulewidth}{0pt}

% first page footer
\fancypagestyle{infofooter}{%
  \fancyhf{}
  \renewcommand\headrulewidth{0pt}
  \fancyfoot[L]{\sffamily\small 
    WORLD MUSIC TEXTBOOK, ISSN: 2767-4215; \copyright~2022, CC-BY-NC-ND\\
    https://doi.org/10.25035/wmt.2022.002}
}
\thispagestyle{infofooter} % remove header from first page
\pagestyle{fancy}     % add headers in other pages

% normal headers
\fancyhead[LO]{\sffamily\small \textbf{\thepage} \quad World Music Textbook}
\fancyhead[RE]{\sffamily\small Nielsen and Vallejo: Music and Nationalism (Annotated Playlist) \quad \textbf{\thepage}}
\fancyfoot{}

% flush left title
\makeatletter
\renewcommand{\maketitle}{\bgroup\setlength{\parindent}{0pt}
\begin{flushleft}
  \vspace*{3\baselineskip}
  \huge{\textbf{\@title}}

  \medskip
  
  \large{\@author}
\end{flushleft}\egroup
}
\makeatother

% for keywords and abstract
\providecommand{\abstracttext}[1]
{
  \noindent
  \textbf{Abstract:} #1
}

\providecommand{\keywords}[1]
{
  \newline
  \textbf{Keywords:} #1
}

% for link
\providecommand{\wmturl}{\href{https://worldmusictextbook.org/nielsen-vallejo-playlist-2022}{https://worldmusictextbook.org/nielsen-vallejo-playlist-2022}}
\providecommand{\wmturltext}{
  \noindent\emph{The online version of this chapter includes all embedded content and is available at \wmturl.}
}

% metadata
\title{Music and Nationalism: Annotated Playlist}
\author{Kristina F. Nielsen | Southern Methodist University\\Jessie Vallejo | Cal Poly Pomona}
% \affil{}

\date{}

% document
\begin{document}
\suppressfloats % prevent float above title
\maketitle

\abstracttext{This is an annotated playlist on the topic of music and nationalism. It can be used to accompany the World Music Textbook article "Music and Nationalism."}
\keywords{activism, global, immigration, nationalism, playlist, politics}

\smallskip

\wmturltext

\medskip

\noindent\hfil\rule{0.5\textwidth}{0.4pt}\hfil

\bigskip

\noindent
This is an annotated playlist on the topic of music and nationalism. It
can be used to accompany the World Music Textbook article
\href{https://worldmusictextbook.org/nielsen-vallejo-2022}{``Music and Nationalism.''} These
pieces are available through most standard music services, but they are
compiled in a Spotify playlist at the link above.

\hypertarget{eight-slavonic-dances-op.-46-no.-1-in-c-presto-by-antonuxedn-dvoux159uxe1k}{%
\subsection*{Eight Slavonic Dances Op. 46: No.~1 in C (Presto) by
Antonín
Dvo\v{r}ák}\label{eight-slavonic-dances-op.-46-no.-1-in-c-presto-by-antonuxedn-dvoux159uxe1k}}

This dance by Dvo\v{r}ák is in the style of folk music. Despite the folkness
it invokes, however, Dvo\v{r}ák composed the melodies himself. This example
is in the form of a dance known as a furiant, which is a fast
triple-meter dance commonly found in Eastern Europe (Anderson 1988;
Tyrrell 2001).~

\hypertarget{sinfonuxeda-india-symphony-no.-2-by-carlos-chuxe1vez}{%
\subsection*{\texorpdfstring{\emph{Sinfonía India} (Symphony No.~2) by
Carlos
Chávez}{Sinfonía India (Symphony No.~2) by Carlos Chávez}}\label{sinfonuxeda-india-symphony-no.-2-by-carlos-chuxe1vez}}

Chávez's \emph{Sinfonía India} draws on musical themes from Indigenous
communities in Mexico and employs Indigenous Mesoamerican percussion
instruments. Written in the mid-1930s, the piece uses European
nationalist strategies of integrating folk music, though Chávez was
culturally distant from the Indigenous communities whose melodies he
borrowed. This compositional strategy reflects Mexican models of
\emph{indigenismo} that co-opted Indigenous culture for nation-building
projects.~~

\hypertarget{pasacalle-by-daniel-alomuxedas-robles}{%
\subsection*{``Pasacalle'' by Daniel Alomías
Robles}\label{pasacalle-by-daniel-alomuxedas-robles}}

In 1913, Peruvian composer Daniel Alomías Robles composed music for the
zarzuela (a form of Spanish musical theater) \emph{El Cóndor Pasa}. In
the vein of other Peruvian works of music and theater at the time, the
piece seized on a nationalist style that employed indigenismo and ideals
of mestizaje, or cultural mixing. The ``Pasacalle'' from this zarzuela
became popularized as the song ``El Cóndor Pasa.'' While Robles was not
Indigenous, his piece quickly became a prominent symbol of Peruvian
Indigeneity and nationalism. Yet the song has also been contested as
some have claimed the melody originated from Indigenous songs that were
also popular in neighboring Bolivia (Mejía 2014; ​​Einarsdóttir and
Hafsdein 2018). The song reflects the complexities of cultures not
fitting neatly with national boundaries and legacies of states
appropriating musical forms for nationalist projects.

\hypertarget{explorations-hi-life-structures-by-the-pan-african-orchestra}{%
\subsection*{``Explorations: Hi-Life Structures'' by The Pan African
Orchestra}\label{explorations-hi-life-structures-by-the-pan-african-orchestra}}

Although based in Ghana, the Pan African Orchestra performs on
instruments from across Africa. Kofi Agawu points out that the music
performed by the Pan African Orchestra often strikes listeners as
traditional even though the pieces they perform are recently composed
(2003:19). This misperception speaks to some of the challenges that
emerge in thinking about what constitutes ``tradition.''~

\hypertarget{oh-susanna-performed-by-the-2nd-south-carolina-string-band}{%
\subsection*{``Oh! Susanna'' performed by the 2nd South Carolina
String
Band}\label{oh-susanna-performed-by-the-2nd-south-carolina-string-band}}

Among the many U.S. songs composed by Stephen Foster, ``Oh! Susanna''
was popularized in minstrel shows across the United States in the
mid-nineteenth century (Root 2013). This version is performed by the 2nd
South Carolina String Band, a group of Civil War reenactors who perform
popular songs from the Civil War era. In the 1920s, automobile tycoon
Henry Ford invested in preserving the composer's home and promoting
Foster's songs alongside those of other composers that he deemed
representative of the country's Anglo musical heritage. A nazi
sympathizer, he sought to distribute and promote music from the
countryside that he saw as culturally and morally superior to other
popular musics (Warnock 2009). The history of this song and others rooted
in minstrel shows have come under renewed scrutiny in places like music
classrooms, where national ideals of inclusivity increasingly confront
racist music histories.

\hypertarget{home-on-the-range-performed-by-dom-flemons}{%
\subsection*{``Home on the Range'' performed by Dom
Flemons}\label{home-on-the-range-performed-by-dom-flemons}}

Sitting around the campfire, the American Western movie \emph{Lone Star
Midnight} (1946), features actor Ken Curtis crooning the song ``Home on
the Range.'' Among the songs most associated with cowboy culture, the
song invokes the emblem of today's quintessential American cowboy---a
figure who is nearly always depicted as white. Despite this popular
portrayal cemented through U.S. popular media there were many Black,
Mexican, and Native American cowboys. The song ``Home on the Range'' was
recorded by American song collector Alan Lomax from an unnamed Black
musician in Texas (Lomax 1945). This song is among the Black cowboy and
folk songs that have been subsumed into U.S. national music while
erasing its Black roots (Flemons 2018).

\hypertarget{polly-anns-hammer-by-our-native-daughters}{%
\subsection*{``Polly Ann's Hammer'' by Our Native
Daughters}\label{polly-anns-hammer-by-our-native-daughters}}

``Polly Ann's Hammer'' was written by Amythyst Kiah and Allison Russell
for Our Native Daughters' 2019 release on Smithsonian Folkways. Whereas
some tracks on this album are covers or reinterpretations and revivals
of Black songs transcribed as early as the 1700s, members of Our Native
Daughters have composed original songs like ``Polly Ann's Hammer'' to
honor and commemorate historical Black figures in the United States who
have often been overlooked. In the case of this track, Kiah and Russell
wrote about the strength of Polly Ann Henry who, prior to 2019, had only
been referenced in some verses of songs about her husband, John Henry
(Nelson 2006).

\hypertarget{el-reluxe1mpago-by-mariachi-herencia-de-muxe9xico}{%
\subsection*{``El Relámpago'' by Mariachi Herencia de
México}\label{el-reluxe1mpago-by-mariachi-herencia-de-muxe9xico}}

Mariachi Herencia de México is a Latin GRAMMY-nominated community
ensemble composed of students from the Chicago area. They are an
outstanding example of how schools and community youth ensembles across
the United States have invested in creating high-profile student
programs that focus on Mexican music. ``El Relámpago'' was released on
their debut album. This track is an arrangement of a standard mariachi
song representative of the genre of traditional sones (polymetric and
polyrhythmic songs) common in Central and Western Mexico.

\hypertarget{he-aloha-nux14d-o-honolulu-composed-by-lot-kauwe-and-performed-by-the-kahauana-lake-trio}{%
\subsection*{``He Aloha Nō 'O Honolulu'' composed by Lot Kauwe and
performed by The Kahauana Lake
Trio}\label{he-aloha-nux14d-o-honolulu-composed-by-lot-kauwe-and-performed-by-the-kahauana-lake-trio}}

Lot Kauwe was a well-known singer, songwriter, and teacher who wrote
songs in the Hawai'ian language during the early 1900s. His song ``He
Aloha Nō 'O Honolulu'' uses metaphors, double-meanings, and Hawai'ian
poetic conventions to sing about love and the Hawai'ian islands (Huapala
1997; Honolulu Advertiser 1922:7). Kahauanu Lake or ``Uncle K'' was a
Hawai'ian scholar, historian, composer, and musician celebrated for his
left-handed ukulele playing. He formed the Kahauana Lake Trio with
bassist Tommy Lake (his brother) and guitarist Al Machida. Kahauana
Lake's music was influential in creating a contemporary Hawai'ian style.
During the second Hawai'ian Renaissance Movement in the 1960s and 1970s,
Lake, his bandmates, and Ma'iki Aiu Lake---an eminent hula dancer,
teacher and his wife---inspired a new generation of Hawai'ian hula
dancers and musicians who resisted colonialism and American imperialism
by promoting traditional Hawai'ian music and dance as well as singing
contemporary music in the Hawai'ian language (Hula Records 2013;
Stillman 1982:49-50).

\hypertarget{sanjuanitos-carabuelapor-el-valle-voypobre-corazuxf3ncarnaval-de-guaranda-performed-by-roberto-zumba}{%
\raggedright
\subsection*{Sanjuanitos: Carabuela/por el Valle Voy/Pobre
Corazón/ \break Carnaval de Guaranda performed by Roberto
Zumba}\label{sanjuanitos-carabuelapor-el-valle-voypobre-corazuxf3ncarnaval-de-guaranda-performed-by-roberto-zumba}}

\rightskip=0pt
\spaceskip=0pt
\xspaceskip=0pt
\relax

Roberto Zumba is regarded as one of Ecuador's founding singers of a
popular music style known as \emph{rocolera} (Wong 2012). Over the
course of his career, Zumba and his contemporaries shifted their singing
styles to reflect discourses about Ecuador's national music. Through
music, they participated in national conversations about racial
identity, ethnicity, social class, and differing generational
perspectives on what it meant to be Ecuadorian. Zumba's medley of
sanjuanito songs draws on Andean Indigenous musical styles and are
performed in the música chicha style featuring synthesizers and
electronic drum tracks. Working-class Ecuadorians began to embrace this
music as a national music style after the 1980s, but middle- and
upper-classes also celebrate some sanjuanito songs, such as ``Pobre
Corazón,'' as a national symbol (heard between 5:05 until 6:32 in this
recording).

\hypertarget{in-the-steppes-of-central-asia-by-alexander-borodin}{%
\subsection*{\texorpdfstring{\emph{In the Steppes of Central Asia} by
Alexander
Borodin}{In the Steppes of Central Asia by Alexander Borodin}}\label{in-the-steppes-of-central-asia-by-alexander-borodin}}

Borodin was among a cohort of Russian composers, known as The Mighty
Five, who sought to create what they perceived as a truly ``Russian''
sound in the late nineteenth century. Borodin sought to paint a sonic
landscape, connecting music to geography---in this case Central Asian
territories that Russia had recently seized in a climate of European
imperialism. The piece draws on musical exoticism, or the portraying
other peoples and places distant from the composer (Locke 2001).~

\hypertarget{sastanuxe0qquxe0m-by-tinariwen}{%
\subsection*{``Sastanàqqàm'' by
Tinariwen}\label{sastanuxe0qquxe0m-by-tinariwen}}

The Tuareg residing in northwestern Africa are an ethnic group with a
shared language and culture, but who do not currently have their own
nation-state. Music provides a means for creating a sense of nationhood
for Tuareg communities residing across different nation-states in
northwestern Africa. The electric guitar has become a particularly
symbolic instrument among the Tuareg since the 1990s (Backer 2015;
Schmidt 2018).

\hypertarget{joyfulness-by-liu-mingyuan-1958}{%
\subsection*{``Joyfulness'' by Liu Mingyuan
(1958)}\label{joyfulness-by-liu-mingyuan-1958}}

\sloppy
This Chinese orchestra piece from 1958 draws on folk music themes and
presents them in an orchestrated format. The modern Chinese orchestra
(MCO) style integrates a range of traditional Chinese instruments that
have historically been played in ensembles like the Silk and Bamboo
ensembles (sizhu) of tea houses. The MCO took its current form in the
1950s as an extension of government efforts to promote a shared Chinese
national heritage and the prevailing ideas of modernization (Kuo-Huang
and Gray 1979:17).~

\hypertarget{the-first-house-in-connaughtthe-copper-plate-reel-by-suxe9amus-ennis}{%
\subsection*{``The First House in Connaught/The Copper Plate Reel'' by
Séamus
Ennis}\label{the-first-house-in-connaughtthe-copper-plate-reel-by-suxe9amus-ennis}}

This reel---a traditional Irish dance form in a quick duple meter---is
among those collected and performed by Séamus Ennis. Ennis collected
thousands of Irish tunes and he performed on the uilleann pipe that is
featured in this example. Ennis gathered music from across Ireland for
the Irish Folklore Commission that, as was the case in many national
projects, saw its role as collecting and preserving Irish traditional
music (Carolan 2001; National Folklore Collection 2022).

\hypertarget{Acknowledgements}{%
\section*{Acknowledgements}\label{Acknowledgements}}

Our thanks to Yuxin Mei and Eric Schmidt for their contributions to this annotated listening guide.

\hypertarget{references-cited}{%
\section*{References cited}\label{references-cited}}

\begin{hangparas}{15pt}{1}

Agawu, Kofi. 2003. \emph{Representing African Music:
Postcolonial Notes, Queries, Positions}. New York: Routledge.

Anderson, Keith. 1988. Liner Notes. \emph{Dvo\v{r}ák Slavonic
Dances} {[}CD{]} Unterhaching-München: Naxos 8.550143.

Backer, Sam. 2015. \href{https://africasacountry.com/2015/03/the-unexpected-popularity-of-dire-straits-in-north-african-tuareg-communities}{``Tuareg Rock.''} \emph{Africa is a Country}, March 27. Accessed February 18, 2022.

Carolan, Nicholas. 2001. ``Ennis, Seamus.'' In \emph{Oxford
Music Online.} Accessed February 18, 2022.~

​​Einarsdóttir, Áslaug, and Valdimar Hafstein. 2018. \href{http://flightofthecondorfilm.com}{\emph{The Flight of the Condor: A Letter, a Song and the Story of Intangible Cultural Heritage}}. Bloomington: Indiana University Press. Film.

Flemons, Dom. 2018. Liner Notes. \href{https://folkways.si.edu/dom-flemons/black-cowboys}{\emph{Dom Flemons presents Black Cowboys}} {[}CD{]}. Washington, D.C.: Smithsonian Folkways Recording SFW CD 40224.

Honolulu Advertiser, The. 1922. \href{https://www.newspapers.com/clip/27904443/1922-lot-kauwe-obituary}{``Lot Kauwe Dies; Was Noted Singer.''} In \emph{The Honolulu Advertiser}, 7, July 16. Accessed February 14, 2022.

Huapala Hawaiian Music and Hula Archives\emph{.} 1997. \href{http://www.huapala.org/Hea/Healoha/_No/_Honolulu.html}{``He Aloha Nō ʻO Honolulu (Goodbye to Honolulu).''} Accessed February 14, 2022.

Hula Records. 2013. \href{http://hularecords.com//_clientData//_ao/_Ecom/obit2.html}{``Hawaiian Icon, Musician Kahauanu Lake Dies.''} Accessed February 14, 2022.

Kuo-Huang, Han, and Judith Gray. 1979. ``The Modern Chinese
Orchestra.'' \emph{Asian Music} 11(1):1-43.

Lomax, John A. 1945. ``Half-Million Dollar Song: Origin of
`Home on the Range.'\,'' \emph{Southwest Review} 31(1):1--8.

Mejía, Luis Salazar. 2014. ``El Cóndor Pasa... y Sus
Misterios.'' \emph{Revista de Crítica Literaria Latinoamericana}
40(80):11--37.

National Folklore Collection. 2022. \href{https://www.duchas.ie/en/info/cbe}{``About.''} Accessed
February 2, 2022.

Nelson, Scott Reynolds. 2006. \emph{Steel Drivin' Man: John
Henry, the Untold Story of an American Legend}. New York: Oxford
University Press.

Root, Deane L. 2013. ``Foster, Stephen C(ollins).'' In
\emph{Oxford Music Online}. Accessed January 27, 2022.~

Schmidt, Eric. 2018. ``Rhythms of Value: Tuareg Music and
Capitalist Reckonings in Niger.'' PhD dissertation, University of
California, Los Angeles.

Stillman, Amy Ku'uleialoha. 1982. ``The Hula Ku'i: A
Tradition in Hawaiian Music and Dance.'' M.A.~thesis, University of
Hawai'i at Manoa.

Tyrrell, John. 2001. ``Furiant.'' In \emph{Oxford Music
Online.} Accessed January 27, 2022.

Wong, Ketty. 2012. \emph{Whose National Music?: Identity,
Mestizaje, and Migration in Ecuador}. Philadelphia: Temple University
Press.

\end{hangparas}
\end{document}