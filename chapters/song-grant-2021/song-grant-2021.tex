\documentclass[twoside]{article}
\usepackage{mathpazo}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage[colorlinks]{hyperref}
\hypersetup{
  colorlinks,
  urlcolor=blue,
  linkcolor=black
}
\usepackage{caption} % unnumbered video examples
% \usepackage{authblk} % author affiliations
\usepackage{hanging} % hanging indents

% headers
\usepackage{fancyhdr}
\renewcommand{\headrulewidth}{0pt}

% first page footer
\fancypagestyle{infofooter}{%
  \fancyhf{}
  \renewcommand\headrulewidth{0pt}
  \fancyfoot[L]{\sffamily\small 
    WORLD MUSIC TEXTBOOK, ISSN: 2767-4215; \copyright~2021, CC-BY-NC-ND\\
    https://doi.org/10.25035/wmt.2021.002}
}
\thispagestyle{infofooter} % remove header from first page
\pagestyle{fancy}     % add headers in other pages

% normal headers
\fancyhead[LO]{\sffamily\small \textbf{\thepage} \quad World Music Textbook}
\fancyhead[RE]{\sffamily\small Song and Grant: Stories of Cambodian Angkuoch \quad \textbf{\thepage}}
\fancyfoot{}

% flush left title
\makeatletter
\renewcommand{\maketitle}{\bgroup\setlength{\parindent}{0pt}
\begin{flushleft}
  \vspace*{3\baselineskip}
  \huge{\textbf{\@title}}

  \medskip
  
  \large{\@author}
\end{flushleft}\egroup
}
\makeatother

% for keywords and abstract
\providecommand{\abstracttext}[1]
{
  \noindent
  \textbf{Abstract:} #1
}

\providecommand{\keywords}[1]
{
  \newline
  \textbf{Keywords:} #1
}

% for link
\providecommand{\wmturl}{\href{https://worldmusictextbook.org/song-grant-2021}{https://worldmusictextbook.org/song-grant-2021}}
\providecommand{\wmturltext}{
  \noindent\emph{The online version of this chapter includes all embedded content and is available at \wmturl.}
}
\providecommand{\wmturlcaption}{
  Scan the QR code or visit \href{https://worldmusictextbook.org/song-grant-2021}{the website} to view examples.
}

% metadata
\title{Stories of Cambodian \emph{Angkuoch}: Documenting a Rare Musical Instrument, its Makers and Players}
\author{SONG Seng | Cambodia\\Catherine GRANT | Griffith University (Australia)}
% \affil{}

\date{}

% document
\begin{document}
\suppressfloats % prevent float above title
\maketitle

\abstracttext{This piece introduces the endangered Cambodian musical instrument Angkuoch, its makers and players, and a project documenting the making process. It also reflects on ethics of instruments in museum collections.}
\keywords{heritage, musical instruments, public musicology, sustainability, Southeast Asia}

\smallskip

\wmturltext

\medskip

\noindent\hfil\rule{0.5\textwidth}{0.4pt}\hfil

\bigskip

The musical instrument popularly known as the ``Jew's Harp'' (also jaw's
harp, mouth harp, trump or \emph{guimbarde}) is found in many countries
around the world. The origin of the name, and of the instrument itself,
remains unclear to scholars, though it is believed it is an `extremely
ancient instrument' that may have originated in Asia, perhaps China (Fox
1988, 22, 49).

The Cambodian version of the instrument is unique. Called
\emph{angkuoch} in Khmer, it is a precious part of Cambodia's living
cultural heritage (Libin 2014; Narom 2005).

Nowadays, angkuoch and its associated practices are in need of urgent
safeguarding. Social and cultural shifts in Cambodia over the last
half-century, including the devastation of the Khmer Rouge era in the
1970s, mean that only a few people still know how to make and play
angkuoch (Miller \& Williams 1998, 204).

Supported by the \emph{Endangered Material Knowledge Program} of the
British Museum (UK) and by UNESCO (Cambodia), in early 2020 we (the
authors) led a team documenting angkuoch and angkuoch-making as it is
practised in Siem Reap Province in northern Cambodia. Our aim was to
help preserve knowledge about angkuoch for present and future
generations.

By briefly introducing the angkuoch makers and players who participated
in this project, and by presenting some of the information they shared
with us during our fieldwork, this article serves as an introduction to
angkuoch. It offers a sense of the changes to angkuoch-making over time,
and the current state of the instrument in its social and cultural
context. It also reflects on one of the key outcomes of the fieldwork:
identifying the likely maker of the angkuoch in the British Museum.

\hypertarget{the-angkuoch-daek}{%
\section*{The Angkuoch Daek}\label{the-angkuoch-daek}}

BIN Song (b. 1942) may be the only living person who still knows how to
make the iron Cambodian angkuoch: angkuoch daek.\footnote{Khmer names
  are usually said and written with the family name first and the given
  name second. In this article, Khmer family names have been written in
  all capital letters for clarity. It is also common that children use
  their father's given name as their family name, which is why KRAK
  Chi's sons are named CHI Chen and CHI Monivong.} He was first
introduced to angkuoch daek as a child, by a man from the neighbouring
Kuy ethnic community. Intrigued, he taught himself how to play angkuoch,
and eventually to make it too. As a young man, he became known for his
skills with the angkuoch.

\begin{figure}
  \includegraphics[width=\textwidth]{../../chapters/song-grant-2021/Figure1.jpg}
  \caption{BIN Song making angkuoch
  daek (iron Jew's harp) (with his wife, standing). Photo: Catherine
  Grant, 8 January 2020.}
\end{figure}

Like artists all over Cambodia, BIN Song stopped making and playing the
angkuoch when the despotic Khmer Rouge came to power in 1975. When the
project team encouraged him in late 2019 to make an angkuoch daek, he
had not made an instrument for nearly fifty years.

At first, BIN Song hesitated to accept our invitation. He was concerned
that his eyesight was no longer up to the task. Moreover, since he had
lost his teeth, he could not test the instrument out as he made it. Yet
he agreed and later he told us: ``I'm getting old now, and when I
reflected on it, I realised it was important to pass this knowledge on
to the next generation. If I don't, who will? If not now, when?''

The British Museum will keep in its collection the angkuoch daek that
BIN Song made for this project, along with video documentation that
shows every step of the process. While BIN Song is happy that people
from all around the world will have a chance to learn about the angkuoch
daek, his foremost message is to young Cambodians: ``do not give up on
angkuoch! This is our culture and heritage!'' He urges:

\begin{quote}
I want to tell people in the next generation that no matter what, you
should certainly play this angkuoch. Learn to play! \ldots{} Keep it
alive! Don't let it be lost!
\end{quote}

\hypertarget{the-language-of-the-angkuoch}{%
\section*{The Language of the
Angkuoch}\label{the-language-of-the-angkuoch}}

SON Soeun and BIN Song (both born 1942) are lifelong friends. In the
1950s and `60s, they grew up together in Preah Ko village in Siem Reap
Province, even entering the monkhood together for a time.

When the young BIN Song began to make and play the angkuoch daek, SON
Soeun became intrigued by the instrument too. Over time, SON Soeun
learnt to play proficiently. In the local lingo, the instrument was
called ``angkuoch bird'' on account of its shape. The two boys would
often slip an angkuoch bird in their pockets to play for pleasure
whenever they chose.

When the two boys were growing up, boys and young men often used the
angkuoch to flirt with girls and young women. Words can be ``spoken''
through the instrument, although it takes some practice to interpret
them (Miller \& Williams 1998, 204). In this way, young people developed
a secret language -- the language of angkuoch. As youths, Song and Souen
would often use the angkuoch for this purpose. SON Soeun recalled:

\begin{quote}
When I was a bachelor, I went to visit the houses of girls at night.
When I reached my lover's house, if she was already asleep, I played to
call her. If I kept calling, she would wake up and come to meet me. And
then we sat together. I could call her to meet me wherever I wanted as
long as she heard the sound of my angkuoch.
\end{quote}

\begin{figure}[ht]
  \includegraphics[width=\textwidth]{../../chapters/song-grant-2021/Figure2.jpg}
  \caption{SON Soeun playing angkuoch
  daek (iron Jew's harp), with his childhood friend, instrument-maker BIN
  Song. Photo: Catherine Grant, 11 January 2020.}
\end{figure}

In addition to its role in wooing potential lovers, angkuoch
traditionally accompanied ``Prern'', a genre of folksong that was
popular in Siem Reap Province at the time BIN Song and SON Soeun were
growing up (Khean et al. 2003). An especially popular Prern song was
``Santouch.'' BIN Song recalled its lyrics for us (in part):

\begin{quote}
Standing near the water pond, if your share with me your rice wine, I
will dance for you to see, my dear.
\end{quote}

Several angkuoch players -- both male and female -- could join in, first
singing and dancing together, then playing angkuoch with one hand and
dancing with the other. People also enjoyed playing and listening to
angkuoch during festivals or special celebrations.

\hypertarget{a-family-of-angkuoch-russey-makers}{%
\section*{A Family of Angkuoch Russey
Makers}\label{a-family-of-angkuoch-russey-makers}}

KRAK Chi (b. 1950) is a rice farmer and bamboo angkuoch-maker from Srah
Srong village, a stone's throw from the world-famous temple complex of
Angkor Wat. KRAK Chi has childhood memories of his father playing the
angkuoch in the evenings for pleasure. Chi also remembers local children
buying the angkuoch russey (a bamboo angkuoch) from instrument-makers in
the village, then selling the instruments on in the dozens to tourists
at the nearby Angkor temples.

\begin{quote}
People played angkuoch in their houses. When they were free, sometimes
they brought it to the rice field and played it right there. If they
lived close to Sras Srong lake, they brought it to play around there.
(KRAK Chi)
\end{quote}

\begin{figure}
  \centering
  \includegraphics[height=4cm]{song-grant-video-1.png}
  \caption{KRAK Chi playing angkuoch russey (bamboo Jew's harp). Video: THON Dika, 5 January 2020. \wmturlcaption}
\end{figure}

As with the iron angkuoch daek, boys and young men liked to use angkuoch
russey to woo lovers. KRAK Chi told us:

\begin{quote}
Boys and girls flirted with each other through the angkuoch. In this
present generation, people use the phone to talk to each other - but at
that time they used the angkuoch.
\end{quote}

KRAK Chi remembers an old custom of putting ``charming wax'' on the
instruments to make sure that any advances that were made via the
angkuoch were irresistible.

KRAK Chi's young son CHI Chen (b. 1988) used to buy angkuoch russey from
the famous angkuoch maker MONG Koeuy in the neighbouring Preah Dak
village, and then sell them on to tourists by the dozen at the famous Ta
Prohm temple near his village Srah Srong. By his teenage years, CHI Chen
had become a proficient player and successful seller of the instruments.

\begin{figure}
  \includegraphics[width=\textwidth]{../../chapters/song-grant-2021/Figure4.jpg}
  \caption{KRAK Chi making angkuoch russey (bamboo Jew's harp). Photo: Catherine Grant, 5 January 2020.}
\end{figure}

Watching MONG Koeuy make the instruments, KRAK Chi decided to learn how
to make them too. He was motivated partly by the prospect of a modest
income from selling the instruments and partly by the desire to keep
alive the tradition of his ancestors. At one stage, KRAK Chi could make
around 15 angkuoch in a day.

KRAK Chi then taught his other son, CHI Monivong (b. 1990), how to make
the instrument. The two brothers, Chen and Monivong, soon became known
as new-generation makers and players of angkuoch russey. However, in
their twenties they found that fewer tourists knew about angkuoch.
Consequently, fewer people were buying the instrument and it became
harder to earn an income by making and selling it. CHI Chen explained:

\begin{quote}
Not many people know about angkuoch anymore. Most who do are old. When​
old people saw me playing at the temple, they said: `Chao (grandchild)!
This is angkuoch. This instrument has existed since the era of our
ancestors.' But younger people said, `Brother! What are you holding? Is
it a wooden pin to fix nets?' I told them: `No, it is an angkuoch.
People use it to make music.' I played for them and they were happy.
\end{quote}

Now Chen and Monivong are in their early thirties with full-time jobs,
and they no longer have much free time for angkuoch-making. However,
they are happy that their father is teaching his two young grandsons how
to make angkuoch. They hope that the family tradition will continue and
develop, and that people of all ages will know and enjoy the angkuoch in
the future.

\hypertarget{mong-koeuy}{%
\section*{MONG Koeuy}\label{mong-koeuy}}

At one time, MONG Koeuy (c.1937-2012) was a renowned angkuoch russey
maker in Siem Reap province. He lived in Preah Dak village, not far from
where KRAK Chi and his sons CHI Monivong and CHI Chen live. It was MONG
Koeuy who introduced the angkuoch to the young CHI Chen, and who later
taught Chen's father KRAK Chi how to make the instrument.

MONG Koeuy learnt to make angkuoch from his father when he was a child.
He sold the instruments he made to tourists at the nearby temples, along
with coconuts and other things. Later, as a young man, MONG also worked
as a farmer and a carpenter.

MONG married his second wife LAV Mech (b. 1945) during the Khmer Rouge
era. He began selling angkuoch again soon after the fall of the Khmer
Rouge, and the income supported their children through school.

\begin{figure}
  \includegraphics[width=\textwidth]{../../chapters/song-grant-2021/Figure5.jpg}
  \caption{Siblings KOEUY Leakhena
  and KOEUY Reatha with their mother LAV Mech (seated), wife of
  angkuoch-maker MONG Koeuy. Photo: Catherine Grant, 17 January 2020.}
\end{figure}

MONG Koeuy passed away in his late 70s in 2012. Four of his sons are
proud to continue their father's tradition.

\hypertarget{the-angkuoch-in-the-british-museum}{%
\section*{The Angkuoch in the British
Museum}\label{the-angkuoch-in-the-british-museum}}

Until the recent acquisition of two new angkuoch through this
project,\footnote{In December 2020, the British Museum acquired two
  angkuoch made for this project: an
  \href{https://www.britishmuseum.org/collection/object/A_2020-3017-2}{angkuoch
  daek} made by BIN Song and an
  \href{https://www.britishmuseum.org/collection/object/A_2020-3017-1}{angkuoch
  russey} made by CHI Monivong.} the British Museum had only
\href{https://www.britishmuseum.org/collection/object/A_As1966-11-5}{one
angkuoch in its collection}, an instrument of unknown maker, donated in
1966 by a certain W Hanson Rawles, about whom nothing further is known.
Today, museum practices carefully document, wherever possible, the
provenance and donation circumstances of items to their collections.

When the research team showed a photo of the Museum instrument donated
by Rawles to the brothers CHI Monivong and CHI Chen and their father
KRAK Chi, they all thought that the instrument bore strong resemblance
to the unique style of MONG Koeuy, especially in its shape, thickness
and length, as well as a characteristic node near the tongue of the
instrument.

When we showed MONG Koeuy's wife LAV Mech and her children Leakhena and
Reatha the photo of the British Museum instrument, they became
emotional. They too recognised features of their father's instruments,
which he had once sold widely to local and foreign tourists. In
Leakhena's words:

\begin{quote}
When I first saw this photo {[}of the British Museum angkuoch{]}, I was
very excited. I never knew my father's craft had been promoted
internationally. Even locally, some people do not even know about it, so
I had not thought it was very prized. Seeing this angkuoch, I miss him.
To my family, the angkuoch symbolises my father.
\end{quote}

On request of the family of MONG Koeuy, the project team is working with
the British Museum to include in its catalogue this new information
about the likely provenance of the instrument. We feel that this is
important for two reasons. First, making this knowledge publicly
available enriches the very limited contextual information known about
this object. Since this object represents angkuoch in the museum's
collection, making accessible the new findings about this specific
instrument will expand the internationally available (English-language)
information about angkuoch in general, and contribute to the
preservation and documentation of knowledge about it for future
generations.

Second, as museums around the world strive to redress past
questionable----or simply plain unethical----practices, adding this new
information to the catalogue will finally duly acknowledge the likely
maker of the object, MONG Koeuy, over 50 years after the British Museum
acquired the instrument. Such an acknowledgement is evidently and
understandably important to the family of MONG Koeuy. While we as
authors hope that this project makes an important contribution to
documenting and safeguarding angkuoch, we also believe that
acknowledging the knowledge and skills of this historical angkuoch-maker
is no less important.

\begin{figure}
  \includegraphics[width=\textwidth]{../../chapters/song-grant-2021/Figure6.jpg}
  \caption{KOEUY Leakhena with a copy
  of the British Museum catalogue entry for the angkuoch russey in its
  collection.}
\end{figure}

\hypertarget{additional-materials}{%
\section*{Additional Materials}\label{additional-materials}}

\hypertarget{acknowledgements}{%
\subsection*{Acknowledgements}\label{acknowledgements}}

This project was funded by the British Museum's
\href{https://www.emkp.org/documentingcambodianmouthharp/}{\emph{Endangered
Material Knowledge Programme}}, supported by Arcadia, a charitable fund
of Lisbet Rausing and Peter Baldwin. UNESCO (Cambodia) provided
additional funding for dissemination of outcomes in Cambodia. We are
grateful to the angkuoch makers and players who shared with us their
knowledge and skills: BIN Song, SON Soeun, KRAK Chi, CHI Monivong, and
CHI Chen. We also acknowledge LAV Mech, KOEUY Leakhena, KOEUY Reatha and
the family of MONG Koeuy, whose story we share here with their
permission. Finally, thanks to research assistant SAY Tola and
videographer THON Dika for their input to the project.

\hypertarget{discussion-questions}{%
\subsection*{Discussion Questions}\label{discussion-questions}}

\hangpara{15pt}{1}
Why do you think it might be important to document knowledge about
instrument-making and instrument-makers? Consider the various types of
people who might wish to access such documentation, now or in the
future.

\begin{itemize}
\item
  Do you know of any other examples of projects that document musical
  instruments, instrument-making, or instrument-makers?
\end{itemize}

\hangpara{15pt}{1}
What ethical considerations arise from housing musical instruments in
museums?

\begin{itemize}
\item
  How did this project relate and respond to one of those
  considerations?
\item
  Can you think of any other ethical considerations arising from this
  project? How might the project team have addressed these?
\end{itemize}

\hypertarget{recommended-readings}{%
\subsection*{Recommended Readings}\label{recommended-readings}}

\hangpara{15pt}{1}
Murphy, Bernice L., ed.~2016. \emph{Museums, Ethics and
Cultural Heritage}. Oxon, UK: Routledge.

\hangpara{15pt}{1}
SAM Sam-Ang. 2008. ``The Khmer People of Cambodia.''
\emph{Garland Handbook of Southeast Asian Music}, 85. New York:
Routledge.

\hangpara{15pt}{1}
Wright, John. 2001.
``\href{https://www.oxfordmusiconline.com/grovemusic/view/10.1093/gmo/9781561592630.001.0001/omo-9781561592630-e-0000014300}{Jew's
harp}.'' \emph{Grove Music Online}. Oxford: Oxford University Press.

\hypertarget{digital-resources}{%
\subsection*{Digital Resources}\label{digital-resources}}

\hangpara{15pt}{1}
\href{https://www.britishmuseum.org/collection/object/A_As1966-11-5}{\emph{Jew's
Harp}}. British Museum.

\begin{quote}
The current catalogue entry for the angkuoch in the British Museum
collection. During the course of this project, the British Museum
updated its online catalogue; the earlier entry we used during fieldwork
in January 2020 (shown in Figure 6) includes a photograph of the asset.
\end{quote}

\hangpara{15pt}{1}
\href{https://www.emkp.org/documentingcambodianmouthharp/}{\emph{Documenting
the Instrument and Instrument-making of Angkuoch, Cambodian Mouth
Harp}}. Endangered Material Knowledge Program, British Museum.

\begin{quote}
The webpage for this project, which (from 2021) will link to the EMKP
Digital Repository, where all project outputs will be freely publicly
available. Outputs include an 18-minute video-documentary, a 24-page
booklet, and nearly 400 files documenting angkuoch-making (photographs,
audio files, video recordings, and interview transcriptions).
\end{quote}

\hangpara{15pt}{1}
\href{https://jewsharpsociety.org/}{\emph{International Jew's
Harp Society}}. \textgreater The website of the International Jew's Harp
Society, including a blog post (30 January 2020) introducing the
angkuoch documentation project.

\hypertarget{works-cited}{%
\section*{Works Cited}\label{works-cited}}

\begin{hangparas}{15pt}{1}

Fox, Leonard. 1988. ``Introduction.'' In Fox, Leonard,
ed.~\emph{The Jew's Harp: A Comprehensive Anthology}, 15-44. Lewisburg:
Bucknell University Press; London: Associated University Presses.

Libin, Laurence. 2014.
``\href{https://www.oxfordmusiconline.com/grovemusic/view/10.1093/gmo/9781561592630.001.0001/omo-9781561592630-e-4002267773}{Aṅkuoc
{[}angkuoch, kangkuoch{]}}.'' \emph{Grove Music Online}. Oxford: Oxford
University Press. Accessed 1 October 2020.

NAROM Keo. 2005. \emph{Cambodian Music}. Phnom Penh: Reyum.

KHEAN Yun, KEO Dorivan, LINA Y., and LENNA Mao, eds.~2003.
\emph{Traditional Musical Instruments of Cambodia, second edition}.
Phnom Penh: United Nations Educational, Scientific and Cultural
Organization.

Miller, Terry E. and Sean Williams, eds.~1998. ``Southeast
Asia: The Khmer People.'' In \emph{Garland Encyclopedia of World Music}
Vol. 4, 151-217. New York: Routledge.

\end{hangparas}

\end{document}