---
title: Music and Archives
author: Ryan Koons
---

**Archive** or **archives**: _Permanently valuable collections of records and the buildings and/or institutions that hold them. “Permanently valuable records” can include letters, reports, manuscripts, photographs, audio and video recordings, and digital-born materials._

Archival science points to archival origins in the 1789-1799 French Revolution. Prior to the Revolution, “archive” referred almost exclusively to documents providing legal or economic privileges to the state, church, nobility, or merchant class. In essence, European privileged classes created safe places for the documents that helped maintain their positions and privilege. The French Revolution began to alter this construction. In keeping with their slogan of “liberty, equality, and fraternity,” the revolutionaries redefined archives as a bulwark of citizens’ rights, ensuring that every citizen could access archival records to protect their rights and freedom (Jimerson 2009).

Archives have been many things to many people. Some have described them as temples, in which records of human activity achieve the semblance of authority and immortality. Referencing the security and protection typically present at an archive, others refer to them as prisons. Still others depict archives as restaurants, where those who are hungry for truth and knowledge seek metaphoric nourishment (Jimerson 2009). More accurately, they contain the very stuff that makes us who we are: evidence of our human life, ideas, truths, failures, and social memory (Kaplan 2000). At their best, archives can function as storehouses containing the tools for self-determination (Seeger 2004). Since materials in archives derive from our everyday lives, they provide us with important perspectives on our pasts/presents to help us decide what we want to do in our futures.

The history of sound and music being included in, or “accessioned” into, archives dates to the invention of sound recording equipment. In 1877, Thomas Edison invented the phonograph, a device that records sound by etching patterns formed by sound waves onto wax cylinders or discs. Soon after, in 1890, U.S. anthropologist Jesse Walter Fewkes (1850-1930) traveled to Maine to the Passamaquoddy Tribe to test this brand-new technology. Over the course of three days and using 35 wax cylinders, he recorded several tribal elders singing songs and speaking vocabulary, numbers, and important cultural narratives. These recordings are the first “field recordings”—recordings made outside of a recording studio—and are now stored at the American Folklife Center at the U.S. Library of Congress.

A few years later, the first sound archives were founded in Europe: the Phonogrammarchiv in the Austrian Academy of Sciences in Vienna in 1899, and the Berliner Phonogramm-Archiv in Berlin in 1900. Importantly, the Berliner Phonogramm-Archiv was directed by Erich von Hornbostel (1877-1935), a pioneering scholar in the discipline now known as “ethnomusicology,” the study of music/sound as culturally meaningful phenomena. At its foundation, ethnomusicology is a discipline based on recordings, and the invention of sound recording technology and the founding of sound archives provided academics with the necessary tools and institutional support to create and develop this new scholarly discipline (Topp-Fargion 2008). Improvements to sound recording technology expanded the discipline. By the 1950s, portable, magnetic recording devices had largely replaced the cumbersome, delicate, and technically demanding phonograph. Suddenly, it was easier and cheaper for field researchers to make high quality recordings. They had soon traveled across the world, recording a vast array of diverse cultures, often placing their field recordings in archives located in Europe and the U.S. In addition to field recordings, sound archives today can contain commercial recordings, radio broadcasts, and materials connected to these recordings, such as personal papers, liner notes, field notes, letters, reports, and release forms.

Today, archivists ensure appropriate care for the collections housed in their repositories so that people can access them. They appraise and select materials of permanent value from donations, store and preserve them in a stable environment, and then arrange them and publish descriptions so that people can find and use them. Preservation typically presents many challenges. Perhaps surprisingly, the archival format with the greatest longevity is paper. However, today many archival materials are digital-born. Digital materials—which include an ever-increasing number of audio and video recordings—are very delicate and exist only as long as the media carrier—like a video tape—containing them. As media and computer hard drives age or break, the files they hold are lost. One solution is to back up materials in triplicate in different locations, which some archival cloud services offer as a standard feature. Nevertheless, digital archivists continue to work towards longer-term solutions to improve our abilities to preserve digital-born materials.

Once perceived as “neutral” caretakers of archives, archivists actually hold substantial power over the archive. And the archive as an institution can itself hold great power within its cultural context. Archival records have been used to define legitimacy, authenticity, and even prove or disprove people’s very existence. Some scholars suggest that events do not necessarily enter into the archive because they are pivotal, but rather that events become pivotal by virtue of being placed in an archive, thereby entering history (Taylor 2006). The archival function of appraisal, in which an archivist chooses some records as having “permanent value” and discards others, determines what people in the future will know about the past: whose voices will be maintained and whose will be silenced. In these regards, archivists co-create the archive (Cook 2011). In a very real sense, even the ways archivists define an “archival format” can be restrictive. In some Polynesian cultures, tattoos are archives (Wright 2009); and in some Caribbean cultures, Carnival performances are archives (Bastian 2009). Yet, body ornamentation and live performances are typically not considered archives by Euro-American archival science and are therefore excluded, disenfranchising these communities.

Repatriation—the return of cultural materials to the country or people who created them—continues to be a simultaneously vital and fraught discussion across cultural memory institutions, including archives. Europe- and U.S.-based ethnomusicologists conducting research in Africa, South and Central America, Australia, Asia, and elsewhere, typically placed their field research materials in archives in Europe and the U.S. It can be prohibitively expensive for source community members to access the holding institutions. For example, the prolific German field recordist Klaus Wachsmann (1907-1984) recorded over 1500 recordings between 1949 and 1957 in Uganda, eventually depositing them at the British Library Sound Archive in the U.K. The people featured in the recordings and their heirs were unable to access them until 2009, when Ugandan ethnomusicologist Sylvia Nannyonga-Tamusuza administered a project to facilitate digital repatriation of accessible copies of Wachsmann’s recordings to Makerere University in Kampala, Uganda’s capital (Nannyonga-Tamusuza and Weintraub 2012). But digital repatriation of copies is not the same thing as repatriating the originals—the repatriating archive does not cede control of the materials. As such, these archives retain control of the cultural heritage materials—and therefore of the cultures—represented in their collections.

Fueled by decolonization and reconciliation movements, some archivists are creating new ways for their repositories to relate with source communities, such as “shared stewardship,” championed by the U.S.-based Smithsonian Institution. This framework recognizes the rights of source communities to control their own cultural heritage. Archivists consult with community members to ensure culturally-appropriate care and description, and share authority, expertise, and responsibility with source communities (Smithsonian Institution Center for Folklife and Culture Heritage 2019). Another toolkit that is increasingly used to promote culturally-appropriate and ethical archival practices, especially in Indigenous communities, is Mukurtu CMS. This free content management system facilitates the creation of community-specific databases and access protocols, for example, by controlling access by time of year, clan, family, age group, sex, etc., using locally meaningful definitions (Christen, Merrill and Wynne 2017).

In summary, archivists around the world work towards the goal of preserving historically meaningful materials—including music and sound—for future generations to access in ethical and meaningful ways. As they do so, they continue to wrestle with the outcomes of colonial and Empire-based histories that their institutions helped construct. Ensuring that the people represented in archives can access the material depicting them means that archives can continue to be useful storehouses of the tools of self-determination.

## Discussion Questions

1. What sounds do you encounter on a regular basis, and how could you go about capturing them so that future generations might be able to hear them, too?

2. What memories and knowledge do you and your communities consider worth saving so that future generations might access them? What format (video, paper, etc.) do they take?

3. How might an archive be useful to the communities in which you are embedded?

## Recommended Reading

Jimerson, Randall. “Introduction: Embracing the Power of Archives.” In _Archives Power: Memory, Accountability, and Social Justice_, 1-23. Chicago: Society of American Archivists, 2009.

Nannyonga-Tamusuza, Sylvia, and Andrew Weintraub. “The Audible Future: Reimagining the Role of Sound Archives and Sound Repatriation in Uganda.” _Ethnomusicology_ 56, no. 2 (2012): 206-233.

## Recommended Repositories

Review the websites and collections of these institutions, which are among some of the best-known sound archives in the world.

- American Folklife Center, Library of Congress

  - Founded in 1976, the Center cares for an archive established in 1928. It is one of the largest ethnographic archives in the world, with millions of items of ethnographic and historical documentation recorded from the 19th century to the present.

- Ethnomusicology Archive, UCLA

  - This sound archive is attached to one of the largest and oldest ethnomusicology graduate programs in the world, and continues to play a vital role in the discipline.

- Ralph Rinzler Folklife Archives and Collections, Center for Folklife and Cultural Heritage, Smithsonian Institution

  - Staff at the Ralph Rinzler Archives created the collaborative collections management framework “shared stewardship” in 2019. Soon after, this framework was adopted by all Smithsonian repositories. In addition to a variety of field recordings, this sound archive includes collections from the Smithsonian Folkways record label, including record labels acquired by Folkways over the years.

- British Museum Sound Archive

  - The first national public museum in the world, the British Museum was established in 1753. The Sound Archive features one of the largest collections of sound recordings in the world.

- Makerere University Klaus Wachsmann Music Archive
  - An excellent example of digital return, or repatriation, this archive, founded in 2009, localizes Ugandan recordings made by Klaus Wachsmann in the mid-1900s in Uganda’s capital.

## Works Cited

Bastian, Jeannette A. “‘Play mas’: carnival in the archives and the archives in carnival: records and community identity in the US Virgin Islands.” _Archival Science_ 9, no. 1 (2009): 113-125.

Christen, Kimberly, Alex Merrill, and Michael Wynne. “A Community of Relations: Mukurtu Hubs and Spokes.” _D-Lib Magazine_ 23 (2017): 5–6.

Cook, Terry. “The Archive(s) Is a Foreign Country: Historians, Archivists, and the Changing Archival Landscape.” _The American Archivist_ 74, no. 2 (2011): 600-632.

Dempsey, Lorcan. “Scientific, Industrial, and Cultural Heritage: a Shared Approach.” _Ariadne_ 22 (2000). <http://www.ariadne.ac.uk/issue/22/dempsey/>.

Jimerson, Randall. _Archives Power: Memory, Accountability, and Social Justice_. Chicago: Society of American Archivists, 2009.

Kaplan, Elizabeth. “We Are What We Collect, We Collect What We Are: Archives and the Construction of Identity.” _The American Archivist_ 63 (2000): 126-151.

Nannyonga-Tamusuza, Sylvia, and Andrew Weintraub. “The Audible Future: Reimagining the Role of Sound Archives and Sound Repatriation in Uganda.” _Ethnomusicology_ 56, no. 2 (2012): 206-233.

Seeger, Anthony. “New Technology Requires New Collaborations: Changing Ourselves to Better Shape the Future.” _Musicology Australia_ 27 (2004): 94-110.

Smithsonian Institution Center for Folklife and Culture Heritage, Shared Stewardship of Collections (July 2019): <https://folklife-media.si.edu/docs/folklife/Shared-Stewardship.pdf>.

Taylor, Diana. “Performance and/As History.” _TDR_ 50, no. 1 (2006): 67-86.

Topp-Fargion, Janet. “Recordings in Context: The Place of Ethnomusicology Archives in the 21st Century.” _IASA Journal_ 30 (2008): 61-65.

Wright, Kirsten. “Recording ‘A Very Particular Custom’: Tattoos and the Archive.” _Archival Science_ 9, no. 1 (2009): 99-111.
