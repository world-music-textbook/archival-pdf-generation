\documentclass[twoside]{article}
\usepackage{mathpazo}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{xurl}
\usepackage[colorlinks]{hyperref}
\hypersetup{
  colorlinks,
  urlcolor=blue,
  linkcolor=black
}
\usepackage{caption} % unnumbered video examples
% \usepackage{authblk} % author affiliations
\usepackage{hanging} % hanging indents

% headers
\usepackage{fancyhdr}
\renewcommand{\headrulewidth}{0pt}

% first page footer
\fancypagestyle{infofooter}{%
  \fancyhf{}
  \renewcommand\headrulewidth{0pt}
  \fancyfoot[L]{\sffamily\small 
    WORLD MUSIC TEXTBOOK, ISSN: 2767-4215; \copyright~2024, CC-BY-NC-ND\\
    https://doi.org/10.25035/wmt.2024.001}
}
\thispagestyle{infofooter} % remove header from first page
\pagestyle{fancy}     % add headers in other pages

% normal headers
\fancyhead[LO]{\sffamily\small \textbf{\thepage} \quad World Music Textbook}
\fancyhead[RE]{\sffamily\small Alexander: Music and Gender \quad \textbf{\thepage}}
\fancyfoot{}

% flush left title
\makeatletter
\renewcommand{\maketitle}{\bgroup\setlength{\parindent}{0pt}
\begin{flushleft}
  \vspace*{3\baselineskip}
  \huge{\textbf{\@title}}

  \medskip
  
  \large{\@author}
\end{flushleft}\egroup
}
\makeatother

% for keywords and abstract
\providecommand{\abstracttext}[1]
{
  \noindent
  \textbf{Abstract:} #1
}

\providecommand{\keywords}[1]
{
  \newline
  \textbf{Keywords:} #1
}

% for link
\providecommand{\wmturl}{\href{https://worldmusictextbook.org/alexander-2024a}{https://worldmusictextbook.org/alexan der-2024a}}
\providecommand{\wmturltext}{
  \noindent\emph{The online version of this chapter is available at \wmturl. This includes video examples of the case studies mentioned here.}
}

% metadata
\title{Music and Gender}
\author{Kathryn Alexander | University of Arizona}
% \affil{}

\date{}

% document
\begin{document}
\suppressfloats % prevent float above title
\maketitle

\abstracttext{This chapter provides an overview of key concepts and terms for the study of music and gender.}
\keywords{music notes, ethnomusicology}

\smallskip

\wmturltext

\medskip

\noindent\hfil\rule{0.5\textwidth}{0.4pt}\hfil

\bigskip

\noindent\textbf{Gender:} a state of experiencing (mis-)alignment with socially
constructed roles, behaviors, expressions, and identities attached to
categories of bodies delineated by a culture's unique criteria. Gender
may hinge on visible anatomy (sex), embodied practices, or other
characteristics.

\hypertarget{introduction-to-gender-and-music}{%
\section*{Introduction to Gender and
Music}\label{introduction-to-gender-and-music}}

As an identity category, gender frames our experience of the world.
Gender is one of the many overlapping, intersecting identities any
individual person holds. But, while we can each define our own gender
identity, our cultures have already set the parameters for which genders
are possible, what those genders look like anatomically, and how those
genders act socially. We can think of gender as something that is built
by a culture, rather than a biological given that stems from anatomy,
chromosomes, and hormones. That gender is culturally constructed means
that the guidelines for how to embody maleness, for example, are
culturally specific and mutable, or capable of changing. What maleness
means, or how individuals identifying as male are expected to look and
act in one culture, differs from how those identifying as male are recognizable in
another culture. The meaning, definition, and experience of gender
varies substantially and differs across time, geography, ideology, and
cultural context.

Broadly, a culture's gender system is used to organize bodies and
behaviors into recognizable and clear categories. In Euro-American
derived cultures, for example, gender has historically been understood
as a binary: male and female. In a binary system, females are socially
women who are expected to adhere to specific behaviors and mannerisms.
They also partake in other social activities that are recognized by the
larger society as distinguishing them as ``women''---or separate from
``men.'' Euro-American society expects individuals to adhere to this
binary system and creates incentives for individuals to conform to these
categories. The often-fulfilled cultural expectation that boys become
men is a kind of normativity; in short, it is normal, expected, and
routine. Cultures often have gendered expectations of musicians that are
based on a culture's ideas of which behaviors and sounds are appropriate
for which gender. For example, there is a persistent association of
gender with Western orchestral instruments, leading to the fact that
boys more often play brass instruments, while girls are encouraged to
play wind instruments (Eros 2008). But this is just one gender system,
one set of cultural scripts, that assigns some social and cultural roles
to one gender and different roles to the other. The following examples
explore various ways that gender influences music and how music reveals
a culture's understandings of gender.

\hypertarget{case-study-1-katajjaq}{%
\section*{Case Study 1: Katajjaq}\label{case-study-1-katajjaq}}

A variety of vocal games exist amongst the different Inuit communities
across North America's Arctic region. Katajjaq, a type of traditional
sonic game, is an Inuit practice of Arctic Québec and the south of
Baffin Island (Nattiez 2016). It features the sounds of the body,
specifically the throat, which is why it is often referred to as
``throat singing.'' Of the many interpretive lenses through which we
could examine katajjaq (including Indigenous sovereignty and settler
colonial oppression), gender pertains here in that the vocal games are
played almost exclusively by women. In this community, life and
culture-sustaining tasks are often gendered male or female, and katajjaq
provides an example of this. Two women, standing in close physical
proximity and often holding each other by the arms, interlock their
voices. The two singers create a specific pattern of intonation as they alternate their breathing in and out, using voiced and unvoiced breath to make sound while inhaling or exhaling. A single
sound might be repeated as a motif (a small musical idea). This motif
usually consists of low and high-pitched components. The game begins as
the motif is slowly varied by the singer who initiated the sound. The
second singer must copy her sound in response, and the game usually ends
in someone out of breath or laughing.

Two practitioners of katajjaq, Karin and Kathy Kettler of the sister duo
Nukariik, report that these were games women played while men were
hunting (Anchorage Daily News 2014); Nattiez adds that boys also learn
these games but traditionally stop participating when they are old
enough to hunt with the adult men (2016). This suggests that the
cultural expectations placed on differently gendered people can change
as they reach adulthood. In this instance, a behavior formerly
appropriate for boys does not typically coincide with the gendered
expectations of them as men (see Singh 2002). It also suggests that
gender identities and the social roles attached to them are, in this
cultural context, clearly defined, and that participation in (or
avoidance of) katajjaq permits adults to indicate their gender identity
and their social roles.

\hypertarget{case-study-2-hawaiian-hula}{%
\section*{Case Study 2: Hawai'ian
Hula}\label{case-study-2-hawaiian-hula}}

Hawai'ian hula is a dance and vocal genre in which~``gestures of the
upper torso and limbs interpret the semantic content of poetic texts
called~\emph{mele}.'' \emph{Mele} tell stories of places, deities,
genealogies, and histories, and are vocally recited while the dance
unfolds. ``Mood and feeling {[}are{]} conveyed through facial
expressions, eye contact and with the hands,'' and the dance is
performed to the accompaniment of rhythms played on a variety of
percussive instruments (Smith, with Stillman 2001). While tourism
promotion campaigns, film and television, novels and comics, and other
popular media have cemented the idea that hula is performed by women,
hula is also performed by men. However, several elements distinguish
male from female hula, including the movement vocabulary, adornments,
facial expressions, and the stories told through dance. The
Euro-American settler colonial conception that dance was feminine eroded
the position of male hula from the early nineteenth century; however,
this belief ignores that fact that the first dancers arriving with voyagers from
Polynesia to the Hawai'ian Islands were male. Indeed, male dancers
(kāne) were primarily responsible for chronicling historical events and
genealogies through hula in traditional Hawai'ian culture (Dimple 2024).

\sloppy 
With colonial incursions into Hawai'i, European and Euro-American gender
ideologies equating dance with femininity led to the belief that men who
performed hula were effeminate. Equating dance with femininity is an
example of how one culture imposed its own gender system onto another
culture---in this case, Euro-American gender beliefs onto those of
Native Hawai'ians (Kānaka Maoli). More troubling still is that this
gender bias intersects with racial bias, serving to strengthen a
racialized and gendered hierarchy that helped serve the interests of
Euro-American colonizers at the expense of Native Ha\-wai'\-ians. European
and Euro-American men could thus be perceived as more ``manly'' than
Hawai'ian men; in short, certain types of masculinity were more
privileged than others. Intersectionality, meaning the compounding
effects of identities in combination (see Crenshaw 1991), allows us to
consider how privilege and its lack operate in fluid, but
culturally-aligned ways: Though masculinity is usually privileged
cross-culturally, here a particular kind of ethnic masculinity was
diminished by Euro-American settlers as they enforced their own
cultures' gender expectations through religious, social, and economic
structures.

We can think about these gender roles as ``gender performativity,''
another tool that can help us assess how gender operates in musical
cultures. The idea that we each perform our gender suggests that we
build our gender identity out of many signifiers that include mannerisms
and clothing to present ourselves to the world on a daily basis.
However, gender theorist Judith Butler uses ``gender performativity'' to
mean something else. They suggest that when we perform our gender, that
performance produces a series of effects (1990; 2011). Butler argues
that we build our gender as we move through the world, and as we do so,
others interact with, perceive, and make decisions about our gender.
Sometimes these perceptions align with the gender we intend for others
to understand, but other times, there are misperceptions. Gender is not
inherent and internal: We are producing it constantly in reference to
our culture's implicit and explicit rules about which genders are
possible and what these genders look, sound, or act like. Gender, in
short, does not exist outside of the cultural scripts, including music,
that make it real and relevant.

\hypertarget{discussion-questions}{%
\section*{Discussion Questions}\label{discussion-questions}}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\item
  What does gender mean to you? Where does this understanding come from?
\item
  How is gender a factor in musical communities with which you are
  involved? This might be as a performer or a consumer of music.
\item
  Make a list of how your gender identity impacts your lived experience
  (for example, think through an average day). Considering one of your
  other identities (such as your ethnic, national, religious, political,
  or generational identity), how does your gender identity operate in
  relationship to that identity?
\end{enumerate}

\hypertarget{recommended-readingmedia}{%
\section*{Recommended Reading/Media}\label{recommended-readingmedia}}
\begin{hangparas}{15pt}{1}

Butler, Judith. ``Judith Butler: Your Behavior Creates Your Gender
\textbar{} Big Think,'' YouTube video, 3:00. Posted by ``Big Think,''
June 6, 2011. \url{https://www.youtube.com/watch?v=Bo7o2LYATDc}.

Kettler, Karin, and Kathy Kettler. ``Karin and Kathy Kettler -- Inuit
Throat-Singing Demonstration {[}Live at First Americans Festival 2004,''
YouTube video, 2:40. Posted by Smithsonian Folkways, February 4, 2010.
\url{https://www.youtube.com/watch?v=8IqOegVCNKI}.

National Museum of the American Indian. ``The Story of the Lū'au: Hula,
an expression of cultural understanding,'' YouTube video, 16:13. Posted
by SmithsonianNMAI, May 12, 2021.
\url{https://www.youtube.com/watch?v=525Jn_7M0aA}.

Crenshaw, Kimberlé. ``The urgency of intersectionality \textbar{}
Kimberlé Crenshaw \textbar{} TED,'' YouTube video, 18:49. Posted by TED,
December 7, 2016. \url{https://www.youtube.com/watch?v=akOe5-UsQ2o}.

Woloshyn, Alexa. 2017. ``\,`Welcome to the Tundra': Tanya Tagaq's
Creative and Communicative Agency as Political Strategy.'' \emph{Journal
of Popular Music Studies} 29(4):1-14.

Flanary, Lisette Marie, dir. 2008. \emph{Nā Kamalei: The Men of Hula}.
DVD. Lehua Films and PBS Hawaii, 60 minutes.

\end{hangparas}
\vspace{16pt} %5mm vertical space
\hypertarget{works-cited}{%
\section*{Works Cited}\label{works-cited}}

\begin{hangparas}{15pt}{1}

Butler, Judith. 1990. \emph{Gender Trouble: Feminism and the Subversion
of Identity}. New York: Routledge.

Crenshaw, Kimberlé. 1991. ``Mapping the Margins: Intersectionality,
Identity Politics, and Violence against Women of Color.'' \emph{Stanford
Law Review} 43(6):1241-1299.

Dimple. 2024. ``Exploring the Artistry and Tradition of Male Hula
Dancers in Hawaii.'' \emph{Hawaii Activities.com}.
\url{https://www.hawaiiactivities.com/travelguide/male-hula}.

Eros, John. 2008. ``Instrument Selection and Gender Stereotypes: A
Review of Recent Literature.'' \emph{Update: Applications of Research in
Music Education} 27(1):57-64.

Kettler, Karin and Kathy Kettler. ``Inuit throat-singing sisters from
Canada,'' YouTube video, 3:03. Posted by ``Anchorage Daily News,''
October 23, 2013.
\url{https://www.youtube.com/watch?v=DLMlkjnYe0U\&t=1s}.

Nattiez, Jean-jacques. 2016. ``Inuit Vocal Games.'' \emph{The Canadian
Encyclopedia}.
\url{https://www.thecanadianencyclopedia.ca/en/article/inuit-vocal-games-emc}.

Singh, Raj. 2002. \emph{The Practice and Performance of} Katajjaq:
\emph{Culture, Identity, and Resistance}. Ph.D.~dissertation. Toronto:
York University.
\url{https://yorkspace.library.yorku.ca/items/2a9914a8-ad36-4aa9-8a63-3d539b9becf3}.

Smith, Barbara. Revised by Amy K. Stillman. 2001. ``Hula.'' \emph{Grove
Music Online}. New York: Oxford University Press.

\end{hangparas}
\end{document}