\documentclass[twoside]{article}
\usepackage{mathpazo}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{xurl}
\usepackage[colorlinks]{hyperref}
\hypersetup{
  colorlinks,
  urlcolor=blue,
  linkcolor=black
}
\usepackage{caption} % unnumbered video examples
% \usepackage{authblk} % author affiliations
\usepackage{hanging} % hanging indents

% headers
\usepackage{fancyhdr}
\renewcommand{\headrulewidth}{0pt}

% first page footer
\fancypagestyle{infofooter}{%
  \fancyhf{}
  \renewcommand\headrulewidth{0pt}
  \fancyfoot[L]{\sffamily\small 
    WORLD MUSIC TEXTBOOK, ISSN: 2767-4215; \copyright~2023, CC-BY-NC-ND\\
    https://doi.org/10.25035/wmt.2023.003}
}
\thispagestyle{infofooter} % remove header from first page
\pagestyle{fancy}     % add headers in other pages

% normal headers
\fancyhead[LO]{\sffamily\small \textbf{\thepage} \quad World Music Textbook}
\fancyhead[RE]{\sffamily\small Llano: Moroccan Trance \quad \textbf{\thepage}}
\fancyfoot{}

% flush left title
\makeatletter
\renewcommand{\maketitle}{\bgroup\setlength{\parindent}{0pt}
\begin{flushleft}
  \vspace*{3\baselineskip}
  \huge{\textbf{\@title}}

  \medskip
  
  \large{\@author}
\end{flushleft}\egroup
}
\makeatother

% for keywords and abstract
\providecommand{\abstracttext}[1]
{
  \noindent
  \textbf{Abstract:} #1
}

\providecommand{\keywords}[1]
{
  \newline
  \textbf{Keywords:} #1
}

% for link
\providecommand{\wmturl}{\href{https://worldmusictextbook.org/llano-2023}{https://worldmusictextbook.org/llano-2023}}
\providecommand{\wmturltext}{
  \noindent\emph{The online version of this chapter is available at \wmturl.}
}
\providecommand{\wmturlcaption}{
  Visit \href{https://worldmusictextbook.org/llano-2023}{the website} to view video examples.
}

% metadata
\title{Moroccan Trance: Unruly Bodies and the Colonial Imagination}
\author{Samuel Llano | University of Manchester}
% \affil{}

\date{}

% document
\begin{document}
\suppressfloats % prevent float above title
\maketitle

\abstracttext{This article considers  colonialism's impact on the production and perpetuation of negative stereotypes about Sufi trance in Morocco by exploring how it developed under strict strutiny and control as an object of fear, misunderstanding, dismissal, and scorn.}
\keywords{Africa, Europe, historical ethnomusicology, nationalism, politics, race}

\smallskip

\wmturltext

\medskip

\noindent\hfil\rule{0.5\textwidth}{0.4pt}\hfil

\bigskip

Trance is a pillar of religion and traditional medicine in Morocco and
the Maghreb. Embraced with fervor by its many followers, dismissed with
scorn or disdain by its critics, and approached with curiosity by
tourists, trance has long been a socially comprehensive
practice.\footnote{I am grateful to the anonymous reviewers for their
  feedback. I would like to thank Christopher Witulski and Kristina
  Nielsen for their thorough comments and insights.} Trance has become
embedded in a range of cultural domains, as it gathers Moroccans from
all walks of life and faiths, including Jewish and Muslim women and
men.\footnote{It is a common misconception that trance in the Maghreb
  involves only Muslim men. See Somer and Saadon 2000.} Many Moroccans
regard trance as a healing remedy capable of freeing those afflicted by
malevolent spirits that cause all types of physical and psychological
ailments. Trance is also a fixture in the gatherings of many Sufi
brotherhoods, where it is used to communicate with the deity.

Sufism is a ``mystical Islamic belief and practice in which Muslims seek
to find the truth of divine love and knowledge through direct personal
experience of God.'' \footnote{\raggedright Schimmel, Annemarie. "Sufism".
  \emph{Encyclopedia Britannica}, 8 May. 2023,
  \href{https://www.britannica.com/topic/Sufism}{https://www.britannica.com/topic/Sufism}. Accessed 19 June 2023.}
Through a variety of mystical paths, Sufis aim to ascertain the nature
of God and bring its presence to the world. As the numbers of followers
of Sufi teachers (\emph{shuyukh}, pl. of \emph{sheikh}) grew in the
beginning of the 12\textsuperscript{th} century, they began to form
larger groups known as brotherhoods who saw their leaders as saints.
Saints are related by kinship to the Prophet Muhammad and are revered
for their austerity, generosity, and good deeds. A \emph{hadra}, seen in a colonial-era image in Figure 1, is a ceremony that
uses ritual dance and animal sacrifice to bestow
\emph{baraka} (godly grace, blessing) upon those suffering from physical
or mental ailments (Witulski 2019, 84--99; Kapchan 2007; Becker 2020,
123--56).

\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{./Figure1.jpg}
  \caption{``Moulay-Idriss: market-place on the day of the ritual dance of the Hamadchas'', from Edith Wharton's \emph{In Morocco} (New York: New York Scribner, 1920). From Wikimedia Commons, public domain, \href{https://commons.wikimedia.org/wiki/File:In_Morocco_(1920)_(14595723677).jpg}{link}}
  \end{figure}
    
Despite enjoying wide popularity, trance in Morocco has often been the
object of fear, misunderstanding, dismissal, or even scorn. Behind these
different responses there has been a good degree of misinformation,
affecting particularly the views of foreigners and outsiders to the
practice. But ignorance or cultural estrangement alone do not explain
the adverse feelings that trance has provoked since at least the late
19\textsuperscript{th} century, and one must look at religious and
political factors for an answer. Sufi trance in colonial Morocco
(1912-1956) developed under the scrutiny and control of the French,
Spanish and Moroccan authorities, and military personnel. It was also
treated as a subject of study within a highly racialized system of
knowledge that considered trance a characteristic manifestation of the
``psychology'' of certain ethnic groups --and by ``psychology'' I mean a
preconceived set of mostly negative traits established through
(pseudo)scholarly practice. Colonial scholars did not observe and study
trance to learn about a cultural practice unknown to them thus far---or
at least not only---but rather mostly to confirm their preconceived
notions about Moroccans of different ethnic subgroups and classes.

\hypertarget{the-hadra}{%
\section*{The Hadra}\label{the-hadra}}

A \emph{hadra} is a Sufi ritual that bestows \emph{baraka} or godly
grace upon those suffering from a particular ailment. It involves the
recitation of religious and secular texts alongside music and bodily
movement to cast away the spirits that inhabit the sufferer and cause
different kinds of physical and psychological ailments. The only
accounts of the \emph{hadra} from this period that are available today
were written by external observers, usually colonial Europeans. Their
writings on trance were published as part of training manuals for the
military officials or within collections of anthropological or
musicological studies. These texts aimed to satisfy a growing curiosity
about then-unknown practices while presenting ethnographic evidence for
their authors' views that western culture and knowledge were superior to
those of non-European lands. This racist intellectual project supported
the notion that Morocco needed to be conquered and civilized. Their
accounts are thus colored by a rhetoric and ideology suited to their
pragmatic and oppressive agendas.\footnote{The fact that none of these
  testimonies reflects upon their authors' presence during the ritual
  further foregrounds their lack of reliability. There is no guarantee
  that what these witnesses saw and heard was not an altered version of
  what the brotherhoods used to perform under regular circumstances,
  that is, when not under direct or strict surveillance. Towards the
  1940s and 50s, Spanish and French officials gained access as
  observants to Islamic rituals in Morocco, but, despite the rhetoric of
  tolerance and mutual understanding that characterized their reports on
  these events, this semblance of intimacy was achieved through a mix of
  coercion and bribes, including donations to build shrines and mosques.
  In addition to these epistemological hurdles, many of these accounts
  were not based on personal experience, but rather they sourced,
  adapted, and reproduced parts of previous accounts which already
  contained semi-fictional episodes and literary tropes added by their
  authors to spice up the narrative.}

In the last few decades, a few scholars have made up for the
shortcomings of this problematic legacy of knowledge by sharing their
personal experience of a \emph{hadra} through self-reflective accounts
grounded on more self-reflective contemporary anthropology (Kapchan
2007; Jankowsky 2021; Witulski 2019; Becker 2020). Their insights on
today's practices cannot be used to describe a \emph{hadra} from the
early twentieth century without discussion though, because any attempt
to extrapolate from them would imply that Sufi ritual trance has
remained static and unchanging. In the light of all these caveats,
describing a \emph{hadra} from the colonial period is necessarily an
exercise in abstraction that risks offering an incomplete or distorted
image of the past.

Using a combination of primary sources from the colonial period and
recent fieldwork, I suggest that the following conclusions can be
established regarding how a \emph{hadra} was performed during the
colonial era. A \emph{hadra} can vary in length, lasting up to eight
hours. It can be a standalone ritual or take place as part of a seasonal
pilgrimage. It is a structured ritual in which each stage is defined by
specific textual and musical materials and by the use of various
recitation techniques and musical textures.\footnote{See Witulski 2019,
  108-12 for a detailed analysis of a \emph{hadra}'s structure and
  sources.} Testimonies by colonial writers rarely if ever discussed any
details of this complex structure. A \emph{hadra} usually ends with an
intensification of pace which may lead one or several participants into
\emph{hal}, the state of trance that captured the attention of most colonial 
writers and photographers (Figure 2). The music contributes to this increase in pace through
repeated musical motives, the addition of instruments, increasing
complexity of the repeated patterns, and a gradual increase in volume.
Bodily movements that accompany these sounds are closely connected to
the recitation and intonation of chants. As a result, the \emph{hadra}
could be considered a kinesthetically-enhanced form of recitation,
rather than a ``dance.'' The writings of colonial settlers or personnel
read this body language in political terms, attributing meanings of
defiance to certain gestures.\footnote{For instance, praying with their
  arms extended and palms facing down was interpreted as a gesture of
  political defiance through which the Tijaniyya brotherhood summoned
  the divine power to chase the Christians away from Morocco
  (Interventor de Beni Uriaguel, 1933).}

\begin{figure}
\centering
\includegraphics[width=\textwidth]{./Figure2.jpg}
\caption{"Aissaoua. Danse du sabre'' from an 1889 postcard, Algeria. Unknown author. From Wikimedia Commons, public domain.}
\end{figure}

\hypertarget{moroccan-trance-in-the-colonial-imagination}{%
\section*{Moroccan Trance in the Colonial
Imagination}\label{moroccan-trance-in-the-colonial-imagination}}

The study of changes in Sufi ritual across time and place raises
important questions about the relationship of ritual trance with its
historical, social, and political context. This was a period of strife
where intense rivalry between the brotherhoods combined with efforts to
maintain themselves despite the pressures put on them by the French and
Spanish colonial authorities. One of the most pressing questions is the
extent to which Sufi trance in colonial Morocco functioned as a
political act. The most comprehensive study of music published in
colonial Morocco, \emph{Tableaux de la Musique Marocaine} by the
Algeria-born French musicologist Alexis Chottin lays out a taxonomy of
music in Morocco (1939). Chottin places the Arab dance music
categorically opposite that of Amazigh communities who had been in the
region before earlier Arab expansion. Although he does not delve
explicitly into political questions, this opposition is closely aligned
with the French authorities' implementation of policies that segregate
these two groups in order to weaken a growing Moroccan nationalism
(Wyrtzen 2016). The most detailed study of Morocco's rural dances by a
Spanish scholar, Emilio Blanco Izaga's \emph{Las danzas rifeñas}
similarly avoids religious or political questions and limits itself to
describing the participants' bodily movements and the structure of the
dances (Blanco Izaga 1946a, 1946b, 1946c). However, the ways in which he
depicts Amazigh communities as ``primitive'' people untainted by
civilization, no matter how well-intentioned, evokes the trope of the
``noble savage'' that had been central in propaganda supporting
colonialism since the Early Modern era.

The question of whether the brotherhoods used trance politically is
impossible to answer given the lack of unbiased scholarship from the
period. Because most writings from the colonial period were written by
military personnel who were concerned with security, they generally
conflate religious practice and politics as they describe trance dancing
as demonic, anti-Christian, and anti-European. Furthermore, they depict
trance as fake---a superstition with no proper place in Islam---and a
dubious remedy that deprived Moroccans of the benefits of European
biomedicine (Padilla 1930, 8; García Figueras 1953, 317). The soldier
and scholar Eduardo Maldonado Vázquez's claim that ``superstition and
the lack of culture attributes curative power to dance'' (Maldonado
Vázquez 1932, 41) was representative of the general state of mind among
the Spanish and the French. Scholars and the military regarded
biomedicine both as a rational counterweight to the frenzy caused by
trance among the Moroccan population and as a weapon against
anti-colonial resistance. Manuel Nido y Torres's manual for officials
argued that the Spanish doctors in Morocco should not restrict
themselves to curing the ``ailments of the body,'' but should treat
those of the ``soul'' too, in order to cast away any ``superstitious
practices,'' ``attract the rebels,'' and carry out a ``pacific
penetration'' of the country (Nido y Torres 1925, 187).

The writings of protectorate scholars and military personnel deployed
these and similar strategies, emptying trance of religious meaning and
political legitimacy. Sufi trance featured recurrently in Spanish
scholarship as a stunning activity that numbed the senses of the
practitioners and interfered with their perception mechanism. The
Spanish described trance as a state that impaired the participants'
cognitive capacities, and in which a wide range of overwhelmingly
intense stimuli saturated their senses, reducing the dancers to a
half-conscious, unresponsive state. The following examples show that the
Spanish in Morocco used their own notions of sonic harmony as indexes of
modernity and ---as did Europeans (Boutin 2015; Llano 2018a, 2018b)---to
formulate ideas of the colonial order that the ``noisy'' dances
threatened to destabilize. Maldonado Vázquez described the \emph{hadra}
as a true cacophony, in which ``The noise of cymbals, drums, tabors, and
flutes,'' mixes with the ``stomping of the feet on the floor,'' the
``shrieks,'' and ``the smell of incense and sweat,'' reducing the
participants to a ``state of torpor,'' in which they ``say all sorts of
stupid things''' (Maldonado Vázquez 1932, 18). Enrique Arqués described
the \emph{hadra} as a ``loud, barbaric music'' whose effect is
comparable to ``the roaring of a tempest falling upon us'' (Arqués 1946,
68).

Such descriptions echo many other comparisons of non-western music with
``noise'' that are commonplace in European writings about their colonial
subjects. Consider however the specific religious connotations of these
testimon\-ies, particularly the denial of spiritual depth implicit in
portraying trance as chaos---Maldonado Vázquez attributed to
``ignorance'' the participants' belief that ``they get closer to the
Deity'' (1932, 18). A condemnatory tone prevailed even in testimonies
that did not discuss or dismiss religious aspects and focused instead on
formal questions, such as the use of repetition. Blanco Izaga, one of
Amazigh culture's greatest champions, described Rifian dances as an ``amorphous
mass,'' because they ``end in the same way that they begin,'' and
because ``there is not a single theme in them to be developed'' (1946b).

\hypertarget{where-the-spanish-got-it-wrong}{%
\section*{Where the Spanish Got it
Wrong}\label{where-the-spanish-got-it-wrong}}

The scholarship and reports produced by the Spanish military and
administration simultaneously blurred and affirmed the distinction
between the real and the imaginary, the body and the mind, and the
physical and the spiritual. This interplay with boundaries was designed
to create epistemological confusion and make it harder to refute views
of curative trance as anything other than a superstition that defiled
sacred religious beliefs. The interpretation of trance by the Spanish is
problematic on at least two counts. First, the brotherhoods do not
inhabit a religious realm that is separate from the secular as described
by western conventional medicine. For them, all is embedded into a
single system of beliefs and practices (Crapanzano 1973, 131). Second,
the presence of supernatural images such as ``six-legged horses''
(Mitjana 1905, 120) in Spanish descriptions of Sufi pilgrimages
compromise the veracity of these testimonies, undermining claims that
trance was pure superstition.

The skepticism of the Spanish was symptomatic of their inability to
understand the nature and purpose of ``medicine'' in Morocco.\footnote{Some
  scholars might object to the use of the term ``medicine,'' preferring
  instead to use ``healing.'' I use ``medicine'' to avoid binaries such
  as scientific/intuitive or modern/primitive that structure comparisons
  between western and Moroccan forms of healing. I follow studies such
  as (Amster 2013) and (Dieste 2010), which distinguish between
  traditional medicine, practiced in Morocco before colonialism, and
  biomedicine, brought by the Europeans.} Morocco's system of healing
draws from a range of traditions, including prophetic medicine, which
searches for answers in the Quran; humoral medicine, which is inspired
in classic Arab medicine; the knowledge of seers, who use magic; the
management of sanctuaries, which administer \emph{baraka}; and
biomedicine, which was introduced by Europeans in Morocco from the early
twentieth century (Dieste 2010, 171). These different traditions do not
exclude each other, but rather coexist within the same medical system,
even within the same treatments. European scholars attributed this
multiplicity to what they judged to be a lack of systematic thinking in
Moroccan society. Europeans failed to appreciate the structural
complexity of both the ritual and the music of Sufi trance. Curative
trance is a system of therapy that is ``a structured set of procedures
for the rehabilitation of an incapacitated individual'' (Crapanzano
1973, 4). The discharge of tension taking place during trance ``is not
merely an emotional outburst {[}...{]} but a highly structured process''
(Crapanzano 1973, 6). Europeans underestimated the complex meaning of
repetition in Sufi trance, dismissing it as a manifestation of a
``primitive'' mentality. Repetition in Sufi trance generates stability
and motion at the same time, because the melodic and rhythmic motives
that are repeated are not fixed, but rather subject to constant
variation and shifting degrees of intensity during the \emph{hadra}
(Jankowsky 2021, 16--17, 29, 40, 51).

\hypertarget{conclusions}{%
\section*{Conclusions}\label{conclusions}}

The colonial history of Sufi trance in Morocco reveals important details
about the ways in which the brotherhoods used dance to negotiate their
identity at a critical moment of their history. The examples discussed
above show that trance was a site of struggle and a tool of identity
construction in the face of colonial oppression and
persecution.\footnote{It is somewhat puzzling that no traces of this
  memory are apparent in today's practice, which rather evokes and
  invokes a more distant past, whether it is the memory of each
  brotherhood's founding saint, or a past of slavery and trans-Saharan
  migration. The reasons for this erasure of memory are partly related
  to the process of de-politization undergone by the brotherhoods
  following the independence of Morocco and its neighbors in the 1950s
  and 60s. The persecution that brotherhoods had experienced has led
  them to seek refuge on the stages of music festivals where they have
  enjoyed greater attention and a more lucrative and stable future. At
  the same time, they have been gradually pushed towards a space that is
  removed from politics where they tend to be regarded as ``folklore.''}
The study of trance's colonial history helps to complicate the genealogy
of this cultural practice by casting its colonial and postcolonial
history through the lens of shifting encounters with power. The medical
history of trance is the history of the body conceived as a site of
struggle that has yet to be written into the history of Moroccan trance.

\hypertarget{discussion-questions}{%
\section*{Discussion Questions}\label{discussion-questions}}

\begin{enumerate}
  \def\labelenumi{\arabic{enumi}.}
  \item
  In what ways do you imagine music contributing to the healing of
  physical and psychological suffering? Consider your own listening
  experiences. Have you ever felt that music had a physical impact upon
  your well-being?
\item
  Why do you think European colonizers regarded Moroccan trance as a
  ``superstition''? Do you believe they simply misinterpreted the
  practice, or do you think there could be additional reasons for
  labeling trance as ``superstition''?
\item
  In what ways might Moroccan trance be considered a ``political''
  practice? What might the study of Moroccan trance teach us about the
  relationship between music and politics? In your discussion consider
  whe\-ther ``political'' has a simple and fixed meaning, or one that is
  complex and contingent on the circumstances in which we define it.
\item
  The only sources that we have documenting earlier practices of
  Moroccan trance are the testimonies left by Europeans. Considering the
  degree to which they are biased and rife with prejudice, to what
  extent do you think they are worthy of our attention? Are there any
  lessons to be drawn from their study? Why do you think it might be
  important to consider the history of Moroccan trance during the
  colonial period rather than focus only on current practices and
  meanings?
\end{enumerate}

\hypertarget{acknowledgements}{%
\section*{Acknowledgements}\label{acknowledgements}}

This research was made possible by funding from the European Research
Council (ERC), as part of the project ``Past and Present Musical
Encounters Across the Strait of Gibraltar'' (MESG-758221).

\hypertarget{references}{%
\section*{References}\label{references}}

\begin{hangparas}{15pt}{1}

Amster, Ellen J. 2013. \emph{Medicine and the Saints: Science, Islam,
and the colonial encounter in Morocco, 1877-1956}. Austin: University of
Texas Press.

Arqués, Enrique. 1946. \emph{El Culto a Los Yenún}. Edited by Etnografía
y Prehistoria Sociedad Española de Antropología. Madrid: Sociedad
Española de Antropología, Etnografía y Prehistoria.

Becker, Cynthia. 2020. \emph{Blackness in Morocco: Gnawa Identity
Through Music and Visual Culture}. Minneapolis: University of Minnesota
Press.

Blanco Izaga, Emilio. 1946a. ``Las Danzas Rifeñas. I: Preámbulo.'' \emph{África:
Revista de Acción Española} 55: 15--16.

------. 1946b. ``Las Danzas Rifeñas. II: Su Interpretación.''
\emph{África: Revista de Acción Española} 56-57: 66--71.

------. 1946c. ``Las Danzas Rifeñas. III.'' \emph{África:
Revista de Acción Española} 5: 55--59.

Boutin, Aimée.~\emph{City of} Noise\emph{: Sound and Nineteenth-Century
Paris}. Chicago: University of Illinois Press, 2015.

Chottin, Alexis. 1939. \emph{Tableau de la musique marocaine}. Paris:
Paul Geuthner.

Crapanzano, Vincent. 1973. \emph{The Hamadsha: A Study in Moroccan
Ethnopsychiatry}. Berkeley: University of California Press.

Dieste, Josep Lluis Mateo. 2010. \emph{Salud y Ritual En Marruecos :
Concepciones del Cuerpo y Prácticas de Curación}. Barcelona: Edicions
Bellaterra.

García Figueras, Tomás. 1953. ``Música y Danza en Marruecos.'' In
\emph{Miscelánea de Estudios Varios Sobre Marruecos}, edited by Tomás
García Figueras, 313--30. Tetuán: Editoria Marroquí.

Interventor de Beni Uriaguel. 1933. ``Letter to the Spanish Consul.''
Archivo General de la Administración. Sección de África. Marruecos. (15)
13, 81/2178 (Cofradías religiosas 1936). Antiguo legajo no. 4483. Caja
A.G.G no. M-2764. Al-Hoceima, 27 April.

Jankowsky, Richard C. 2021. \emph{Ambient Sufism: Ritual Niches and the
Social Work of Musical Form}. First edition. Chicago: University of
Chicago Press.

Kahn, Douglas. 2001. \emph{Noise, Water, Meat: A History of Voice,
Sound, and Aurality in the Arts}. Cambridge: MIT Press.

Kapchan, Deborah. 2007. \emph{Traveling Spirit Masters: Moroccan Gnawa
Trance and Mu\-sic in the Global Marketplace}. Middletown, CT: Wesleyan
Universi\-ty Press.

Llano, Samuel. 2018a. \emph{Discordant Notes: Marginality and Social
Control in Madrid, 1850-1930}. New York: Oxford University Press.

------. 2018b. ``Mapping Street Sounds in the Nineteenth-Century
City: A Listener's Guide to Social Engineering.'' \emph{Sound Studies} 4
(2): 143--61.

Maldonado Vázquez, Eduardo. 1932. \emph{Cofradías Religiosas En
Marruecos, I. Curso de Perfeccionamiento de Oficiales de Servicio de
Intervención: Subtitle}. Tetuán: Alta Comisaría de la República Española
en Marruecos.

Mitjana, Rafael. 1905. \emph{En el Magreb-el-Aksa}. Valencia: Sempere y
Compañía.

Nido y Torres, Manuel. 1925. \emph{Apuntes Para El Oficial de
Intervención Y de Tropas Coloniales}. Tetuán: Editorial Hispano
Africana.

Padilla, Comandante R. 1930. \emph{Cofradías Religiosas en el Rif y
Diversas Taifas de Xorfas. Zauias Y Santuarios}. Tetuán: Alta Comisaría
de España en Marruecos.

Somer, Eli, and Meir Saadon. 2000. ``Stambali: Dissociative Possession
and Trance in a Tunisian Healing Dance.'' \emph{Transcultural
Psychiatry} 37 (4): 580--600.

Witulski, Christopher. 2019. \emph{Focus: Music and Religion of
Morocco}. New York: Routledge.

Wyrtzen, Jonathan.~Making Morocco: Colonial intervention and the
politics of identity. Ithaca: Cornell University Press, 2016.

\end{hangparas}
\end{document}