---
title: "Moroccan Trance: Unruly Bodies and the Colonial Imagination"
author: "Samuel Llano"
---

Biography: Samuel Llano teaches the cultural history and music of Spain and the
western Mediterranean in the University of Manchester. He is the author
of _Whose Spain? Negotiating \'Spanish Music\' in Paris, 1908-1929_
(2012), winner of the Robert M. Stevenson Award of the American
Musicological Society; and _Discordant Notes: Marginality and Social
Control in Madrid, 1850-1930_ (2018). He is currently writing a book
titled _The Empire of the Ear: Music, Race, and the Sonic Architecture
of Colonial Morocco_.

Samuel Llano, University of Manchester

Abstract: Sufi trance in colonial Morocco developed under strict colonial scrutiny and control and has been the object of fear, misunderstanding, dismissal, and even scorn. This article considers colonialism's impact on the production and perpetuation of negative stereotypes about trance.

Keywords: trance, colonialism, Morocco, Spain, race

**Samuel** **Llano, University of Manchester**

## Introduction

Trance is a pillar of religion and traditional medicine in Morocco and
the Maghreb. Embraced with fervor by its many followers, dismissed with
scorn or disdain by its critics, and approached with curiosity by
tourists, trance has long been a socially comprehensive practice.[^1]
Trance has become embedded in a range of cultural domains, as it gathers
Moroccans from all walks of life and faiths, including Jewish and Muslim
women and men.[^2] Many Moroccans regard trance as a healing remedy
capable of freeing those afflicted by malevolent spirits that cause all
types of physical and psychological ailments. Trance is also a fixture
in the gatherings of many Sufi brotherhoods, where it is used to
communicate with the deity.

Sufism is a "mystical Islamic belief and practice in which Muslims seek
to find the truth of divine love and knowledge through direct personal
experience of God." [^3] Through a variety of mystical paths, Sufis aim
to ascertain the nature of God and bring its presence to the world. As
the numbers of followers of Sufi teachers (_shuyukh_, pl. of _sheikh_)
grew in the beginning of the 12^th^ century, they began to form larger
groups known as brotherhoods who saw their leaders as saints. Saints are
related by kinship to the Prophet Muhammad and are revered for their
austerity, generosity, and good deeds. A _hadra_, seen in a colonial-era image in Figure 1, is a ceremony that
uses ritual dance and animal sacrifice to bestow _baraka_ (godly
grace, blessing) upon those suffering from physical or mental ailments
(Witulski 2019, 84--99; Kapchan 2007; Becker 2020, 123--56).

![Figure 1: “Moulay-Idriss: market-place on the day of the ritual dance of the Hamadchas”, from Edith Wharton’s *In Morocco* (New York: New York Scribner, 1920). From Wikimedia Commons, public domain, [link](https://commons.wikimedia.org/wiki/File:In_Morocco_(1920)_(14595723677).jpg)](./Figure1.jpg)

Despite enjoying wide popularity, trance in Morocco has often been the
object of fear, misunderstanding, dismissal, or even scorn. Behind these
different responses there has been a good degree of misinformation,
affecting particularly the views of foreigners and outsiders to the
practice. But ignorance or cultural estrangement alone do not explain
the adverse feelings that trance has provoked since at least the late
19^th^ century, and one must look at religious and political factors for
an answer. Sufi trance in colonial Morocco (1912-1956) developed under
the scrutiny and control of the French, Spanish and Moroccan
authorities, and military personnel. It was also treated as a subject of
study within a highly racialized system of knowledge that considered
trance a characteristic manifestation of the "psychology" of certain
ethnic groups --and by "psychology" I mean a preconceived set of mostly
negative traits established through (pseudo)scholarly practice. Colonial
scholars did not observe and study trance to learn about a cultural
practice unknown to them thus far---or at least not only---but rather
mostly to confirm their preconceived notions about Moroccans of
different ethnic subgroups and classes.

## The Hadra

A _hadra_ is a Sufi ritual that bestows _baraka_ or godly grace upon
those suffering from a particular ailment. It involves the recitation of
religious and secular texts alongside music and bodily movement to cast
away the spirits that inhabit the sufferer and cause different kinds of
physical and psychological ailments. The only accounts of the _hadra_
from this period that are available today were written by external
observers, usually colonial Europeans. Their writings on trance were
published as part of training manuals for the military officials or
within collections of anthropological or musicological studies. These
texts aimed to satisfy a growing curiosity about then-unknown practices
while presenting ethnographic evidence for their authors' views that
western culture and knowledge were superior to those of non-European
lands. This racist intellectual project supported the notion that
Morocco needed to be conquered and civilized. Their accounts are thus
colored by a rhetoric and ideology suited to their pragmatic and
oppressive agendas.[^4]

In the last few decades, a few scholars have made up for the
shortcomings of this problematic legacy of knowledge by sharing their
personal experience of a _hadra_ through self-reflective accounts
grounded on more self-reflective contemporary anthropology (Kapchan
2007; Jankowsky 2021; Witulski 2019; Becker 2020). Their insights on
today's practices cannot be used to describe a _hadra_ from the early
twentieth century without discussion though, because any attempt to
extrapolate from them would imply that Sufi ritual trance has remained
static and unchanging. In the light of all these caveats, describing a
_hadra_ from the colonial period is necessarily an exercise in
abstraction that risks offering an incomplete or distorted image of the
past.

Using a combination of primary sources from the colonial period and
recent fieldwork, I suggest that the following conclusions can be
established regarding how the _hadra_ was performed during the colonial
era. A _hadra_ can vary in length, lasting up to eight hours. It can be
a standalone ritual or take place as part of a seasonal pilgrimage. It
is a structured ritual in which each stage is defined by specific
textual and musical materials and by the use of various recitation
techniques and musical textures.[^5] Testimonies by colonial writers
rarely if ever discussed any details of this complex structure. A
_hadra_ usually ends with an intensification of pace which may lead one
or several participants into _hal_, the state of trance that captured the attention of most colonial writers and photographers (Figure 2). The music contributes to this
increase in pace through repeated musical motives, the addition of
instruments, increasing complexity of the repeated patterns, and a
gradual increase in volume. Bodily movements that accompany these sounds
are closely connected to the recitation and intonation of chants. As a
result, the _hadra_ could be considered a kinesthetically-enhanced form
of recitation, rather than a "dance." The writings of colonial settlers
or personnel read this body language in political terms, attributing
meanings of defiance to certain gestures.[^6]

![Figure 2: "Aissaoua. Danse du sabre” from an 1889 postcard, Algeria. Unknown author. From Wikimedia Commons, public domain, [link](https://commons.wikimedia.org/wiki/File:Fotografi_p%C3%A5_Aissawa_och_sabeldans,_Alger_-_Hallwylska_museet_-_107932.tif)](./Figure2.jpg)

## Moroccan Trance in the Colonial Imagination

The study of changes in Sufi ritual across time and place raises
important questions about the relationship of ritual trance with its
historical, social, and political context. This was a period of strife
where intense rivalry between the brotherhoods combined with efforts to
maintain themselves despite the pressures put on them by the French and
Spanish colonial authorities. One of the most pressing questions is the
extent to which Sufi trance in colonial Morocco functioned as a
political act. The most comprehensive study of music published in
colonial Morocco, _Tableaux de la Musique Marocaine_ by the Algeria-born
French musicologist Alexis Chottin lays out a taxonomy of music in
Morocco (1939). Chottin places the Arab dance music categorically
opposite that of Amazigh communities who had been in the region before
earlier Arab expansion. Although he does not delve explicitly into
political questions, this opposition is closely aligned with the French
authorities' implementation of policies that segregate these two groups
in order to weaken a growing Moroccan nationalism (Wyrtzen 2016). The
most detailed study of Morocco's rural dances by a Spanish scholar,
Emilio Blanco Izaga's _Las danzas rifeñas_ similarly avoids religious or
political questions and limits itself to describing the participants'
bodily movements and the structure of the dances (Blanco Izaga 1946a,
1946b, 1946c). However, the ways in which he depicts Amazigh communities
as "primitive" people untainted by civilization, no matter how
well-intentioned, evokes the trope of the "noble savage" that had been
central in propaganda supporting colonialism since the Early Modern era.

The question of whether the brotherhoods used trance politically is
impossible to answer given the lack of unbiased scholarship from the
period. Because most writings from the colonial period were written by
military personnel who were concerned with security, they generally
conflate religious practice and politics as they describe trance dancing
as demonic, anti-Christian, and anti-European. Furthermore, they depict
trance as fake---a superstition with no proper place in Islam---and a
dubious remedy that deprived Moroccans of the benefits of European
biomedicine (Padilla 1930, 8; García Figueras 1953, 317). The soldier
and scholar Eduardo Maldonado Vázquez's claim that "superstition and the
lack of culture attributes curative power to dance" (Maldonado Vázquez
1932, 41) was representative of the general state of mind among the
Spanish and the French. Scholars and the military regarded biomedicine
both as a rational counterweight to the frenzy caused by trance among
the Moroccan population and as a weapon against anti-colonial
resistance. Manuel Nido y Torres's manual for officials argued that the
Spanish doctors in Morocco should not restrict themselves to curing the
"ailments of the body," but should treat those of the "soul" too, in
order to cast away any "superstitious practices," "attract the rebels,"
and carry out a "pacific penetration" of the country (Nido y Torres
1925, 187).

The writings of protectorate scholars and military personnel deployed
these and similar strategies, emptying trance of religious meaning and
political legitimacy. Sufi trance featured recurrently in Spanish
scholarship as a stunning activity that numbed the senses of the
practitioners and interfered with their perception mechanism. The
Spanish described trance as a state that impaired the participants'
cognitive capacities, and in which a wide range of overwhelmingly
intense stimuli saturated their senses, reducing the dancers to a
half-conscious, unresponsive state. The following examples show that the
Spanish in Morocco used their own notions of sonic harmony as indexes of
modernity and ---as did Europeans (Boutin 2015; Llano 2018a, 2018b)---to
formulate ideas of the colonial order that the "noisy" dances threatened
to destabilize. Maldonado Vázquez described the _hadra_ as a true
cacophony, in which "The noise of cymbals, drums, tabors, and flutes,"
mixes with the "stomping of the feet on the floor," the "shrieks," and
"the smell of incense and sweat," reducing the participants to a "state
of torpor," in which they "say all sorts of stupid things"' (Maldonado
Vázquez 1932, 18). Enrique Arqués described the _hadra_ as a "loud,
barbaric music" whose effect is comparable to "the roaring of a tempest
falling upon us" (Arqués 1946, 68).

Such descriptions echo many other comparisons of non-western music with
"noise" that are commonplace in European writings about their colonial
subjects. Consider however the specific religious connotations of these
testimonies, particularly the denial of spiritual depth implicit in
portraying trance as chaos---Maldonado Vázquez attributed to "ignorance"
the participants' belief that "they get closer to the Deity" (1932, 18).
A condemnatory tone prevailed even in testimonies that did not discuss
or dismiss religious aspects and focused instead on formal questions,
such as the use of repetition. Blanco Izaga, one of Amazigh culture's
greatest champions, described Rifian dances as an "amorphous mass," because they
"end in the same way that they begin," and because "there is not a
single theme in them to be developed" (1946b).

## Where the Spanish Got it Wrong

The scholarship and reports produced by the Spanish military and
administration simultaneously blurred and affirmed the distinction
between the real and the imaginary, the body and the mind, and the
physical and the spiritual. This interplay with boundaries was designed
to create epistemological confusion and make it harder to refute views
of curative trance as anything other than a superstition that defiled
sacred religious beliefs. The interpretation of trance by the Spanish is
problematic on at least two counts. First, the brotherhoods do not
inhabit a religious realm that is separate from the secular as described
by western conventional medicine. For them, all is embedded into a
single system of beliefs and practices (Crapanzano 1973, 131). Second,
the presence of supernatural images such as "six-legged horses" (Mitjana
1905, 120) in Spanish descriptions of Sufi pilgrimages compromise the
veracity of these testimonies, undermining claims that trance was pure
superstition.

The skepticism of the Spanish was symptomatic of their inability to
understand the nature and purpose of "medicine" in Morocco.[^7]
Morocco's system of healing draws from a range of traditions, including
prophetic medicine, which searches for answers in the Quran; humoral
medicine, which is inspired in classic Arab medicine; the knowledge of
seers, who use magic; the management of sanctuaries, which administer
_baraka_; and biomedicine, which was introduced by Europeans in Morocco
from the early twentieth century (Dieste 2010, 171). These different
traditions do not exclude each other, but rather coexist within the same
medical system, even within the same treatments. European scholars
attributed this multiplicity to what they judged to be a lack of
systematic thinking in Moroccan society. Europeans failed to appreciate
the structural complexity of both the ritual and the music of Sufi
trance. Curative trance is a system of therapy that is "a structured set
of procedures for the rehabilitation of an incapacitated individual"
(Crapanzano 1973, 4). The discharge of tension taking place during
trance "is not merely an emotional outburst \[\...\] but a highly
structured process" (Crapanzano 1973, 6). Europeans underestimated the
complex meaning of repetition in Sufi trance, dismissing it as a
manifestation of a "primitive" mentality. Repetition in Sufi trance
generates stability and motion at the same time, because the melodic and
rhythmic motives that are repeated are not fixed, but rather subject to
constant variation and shifting degrees of intensity during the _hadra_
(Jankowsky 2021, 16--17, 29, 40, 51).

## Conclusions

The colonial history of Sufi trance in Morocco reveals important details
about the ways in which the brotherhoods used dance to negotiate their
identity at a critical moment of their history. The examples discussed
above show that trance was a site of struggle and a tool of identity
construction in the face of colonial oppression and persecution.[^8] The
study of trance's colonial history helps to complicate the genealogy of
this cultural practice by casting its colonial and postcolonial history
through the lens of shifting encounters with power. The medical history of
trance is the history of the body conceived as a site of struggle that
has yet to be written into the history of Moroccan trance.

## Discussion questions

- In what ways do you imagine music contributing to the healing of
  physical and psychological suffering? Consider your own listening
  experiences. Have you ever felt that music had a physical impact
  upon your well-being?

- Why do you think European colonizers regarded Moroccan trance as a
  "superstition"? Do you believe they simply misinterpreted the
  practice, or do you think there could be additional reasons for
  labeling trance as "superstition"?

- In what ways might Moroccan trance be considered a "political"
  practice? What might the study of Moroccan trance teach us about the
  relationship between music and politics? In your discussion consider
  whether "political" has a simple and fixed meaning, or one that is
  complex and contingent on the circumstances in which we define it.

- The only sources that we have documenting earlier practices of
  Moroccan trance are the testimonies left by Europeans. Considering
  the degree to which they are biased and rife with prejudice, to what
  extent do you think they are worthy of our attention? Are there any
  lessons to be drawn from their study? Why do you think it might be
  important to consider the history of Moroccan trance during the
  colonial period rather than focus only on current practices and
  meanings?

## Acknowledgements

This research was made possible by funding from the European
Research Council (ERC), as part of the project "Past and Present Musical
Encounters Across the Strait of Gibraltar" (MESG-758221).

## References

Amster, Ellen J. 2013. _Medicine and the Saints: Science, Islam, and the
colonial encounter in Morocco, 1877-1956_. Austin: University of Texas
Press.

Arqués, Enrique. 1946. _El Culto a Los Yenún_. Edited by Etnografía y
Prehistoria Sociedad Española de Antropología. Madrid: Sociedad Española
de Antropología, Etnografía y Prehistoria.

Becker, Cynthia. 2020. _Blackness in Morocco: Gnawa Identity Through
Music and Visual Culture_. Minneapolis: University of Minnesota Press.

Blanco Izaga, Emilio. 1946c. "Las Danzas Rifeñas. I: Preámbulo." _África: Revista
de Acción Española_ 55: 15--16.

---------. 1946b. "Las Danzas Rifeñas. II: Su Interpretación." _África:
Revista de Acción Española_ 56-57: 66--71.

---------. 1946a. "Las Danzas Rifeñas. III." _África: Revista
de Acción Española_ 5: 55--59.

Boutin, Aimée. *City of* Noise*: Sound and Nineteenth-Century Paris*.
Chicago: University of Illinois Press, 2015.

Chottin, Alexis. 1939. _Tableau de la musique marocaine_. Paris: Paul
Geuthner.

Crapanzano, Vincent. 1973. _The Hamadsha: A Study in Moroccan
Ethnopsychiatry_. Berkeley: University of California Press.

Dieste, Josep Lluis Mateo. 2010. _Salud y Ritual en Marruecos :
Concepciones del Cuerpo y Prácticas de Curación_. Barcelona: Edicions
Bellaterra.

García Figueras, Tomás. 1953. "Música y Danza En Marruecos." In
_Miscelánea de Estudios Varios Sobre Marruecos_, edited by Tomás García
Figueras, 313--30. Tetuán: Editoria Marroquí.

Interventor de Beni Uriaguel. 1933. "Letter to the Spanish Consul."
Archivo General de la Administración. Sección de África. Marruecos. (15)
13, 81/2178 (Cofradías religiosas 1936). Antiguo legajo no. 4483. Caja
A.G.G no. M-2764. Al-Hoceima, 27 April.

Jankowsky, Richard C. 2021. _Ambient Sufism: Ritual Niches and the
Social Work of Musical Form_. First edition. Chicago: University of
Chicago Press.

Kahn, Douglas. 2001. _Noise, Water, Meat: A History of Voice, Sound, and
Aurality in the Arts_. Cambridge: MIT Press.

Kapchan, Deborah. 2007. _Traveling Spirit Masters: Moroccan Gnawa Trance
and Music in the Global Marketplace_. Middletown, CT: Wesleyan
University Press.

Llano, Samuel. 2018a. _Discordant Notes: Marginality and Social Control
in Madrid, 1850-1930_. New York: Oxford University Press.

---------. 2018b. "Mapping Street Sounds in the Nineteenth-Century City:
A Listener's Guide to Social Engineering." _Sound Studies_ 4 (2):
143--61.

Maldonado Vázquez, Eduardo. 1932. _Cofradías Religiosas En Marruecos, I.
Curso de Perfeccionamiento de Oficiales de Servicio de Intervención:
Subtitle_. Tetuán: Alta Comisaría de la República Española en Marruecos.

Mitjana, Rafael. 1905. _En el Magreb-el-Aksa_. Valencia: Sempere y
Compañía.

Nido y Torres, Manuel. 1925. _Apuntes Para El Oficial de Intervención Y
de Tropas Coloniales_. Tetuán: Editorial Hispano Africana.

Padilla, Comandante R. 1930. _Cofradías Religiosas en el Rif y Diversas
Taifas de Xorfas. Zauias Y Santuarios_. Tetuán: Alta Comisaría de España
en Marruecos.

Somer, Eli, and Meir Saadon. 2000. "Stambali: Dissociative Possession
and Trance in a Tunisian Healing Dance." _Transcultural Psychiatry_ 37
(4): 580--600.

Witulski, Christopher. 2019. _Focus: Music and Religion of Morocco_. New
York: Routledge.

Wyrtzen, Jonathan. Making Morocco: Colonial intervention and the
politics of identity. Ithaca: Cornell University Press, 2016.

[^1]:
    I am grateful to the anonymous reviewers for their feedback. I
    would like to thank Christopher Witulski and Kristina Nielsen for
    their thorough comments and insights.

[^2]:
    It is a common misconception that trance in the Maghreb involves
    only Muslim men. See Somer and Saadon 2000.

[^3]:
    Schimmel, Annemarie. \"Sufism\". _Encyclopedia Britannica_, 8 May.
    2023, https://www.britannica.com/topic/Sufism. Accessed 19 June 2023.

[^4]:
    The fact that none of these testimonies reflects upon their
    authors' presence during the ritual further foregrounds their lack
    of reliability. There is no guarantee that what these witnesses saw
    and heard was not an altered version of what the brotherhoods used
    to perform under regular circumstances, that is, when not under
    direct or strict surveillance. Towards the 1940s and 50s, Spanish
    and French officials gained access as observants to Islamic rituals
    in Morocco, but, despite the rhetoric of tolerance and mutual
    understanding that characterized their reports on these events, this
    semblance of intimacy was achieved through a mix of coercion and
    bribes, including donations to build shrines and mosques. In
    addition to these epistemological hurdles, many of these accounts
    were not based on personal experience, but rather they sourced,
    adapted, and reproduced parts of previous accounts which already
    contained semi-fictional episodes and literary tropes added by their
    authors to spice up the narrative.

[^5]:
    See Witulski 2019, 108-12 for a detailed analysis of a _hadra_'s
    structure and sources.

[^6]:
    For instance, praying with their arms extended and palms facing
    down was interpreted as a gesture of political defiance through
    which the Tijaniyya brotherhood summoned the divine power to chase
    the Christians away from Morocco (Interventor de Beni Uriaguel,
    1933).

[^7]:
    Some scholars might object to the use of the term "medicine,"
    preferring instead to use "healing." I use "medicine" to avoid
    binaries such as scientific/intuitive or modern/primitive that structure
    comparisons between western and Moroccan forms of healing. I follow
    studies such as (Amster 2013) and (Dieste 2010), which distinguish
    between traditional medicine, practiced in Morocco before
    colonialism, and biomedicine, brought by the Europeans.

[^8]:
    It is somewhat puzzling that no traces of this memory are apparent
    in today's practice, which rather evokes and invokes a more distant
    past, whether it is the memory of each brotherhood's founding saint,
    or a past of slavery and trans-Saharan migration. The reasons for
    this erasure of memory are partly related to the process of
    de-politization undergone by the brotherhoods following the
    independence of Morocco and its neighbors in the 1950s and 60s. The
    persecution that brotherhoods had experienced has led them to seek
    refuge on the stages of music festivals where they have enjoyed
    greater attention and a more lucrative and stable future. At the
    same time, they have been gradually pushed towards a space that is
    removed from politics where they tend to be regarded as "folklore."
